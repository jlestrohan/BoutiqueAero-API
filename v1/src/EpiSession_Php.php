<?php

class EpiSession_Php implements EpiSessionInterface
{
    public function end()
    {
        $_SESSION = array();

        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time() - 42000, '/');
        }

        session_destroy();
    }

    public function get($key = NULL)
    {
        if (empty($key) || !isset($_SESSION[$key])) {
            return FALSE;
        }

        return $_SESSION[$key];
    }

    public function set($key = NULL, $value = NULL)
    {
        if (empty($key)) {
            return FALSE;
        }

        $_SESSION[$key] = $value;
        return $value;
    }

    public function __construct()
    {
        if (!session_id()) {
            session_start();
        }
    }
}

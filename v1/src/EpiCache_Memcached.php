<?php

class EpiCache_Memcached extends EpiCache
{
    private static $connected = FALSE;
    private $memcached = NULL;
    private $host = NULL;
    private $port = NULL;
    private $compress = NULL;
    private $expiry = NULL;

    public function __construct($params = array())
    {
        $this->host = !empty($params[0]) ? $params[0] : 'localhost';
        $this->port = !empty($params[1]) ? $params[1] : 11211;;
        $this->compress = !empty($params[2]) ? $params[2] : 0;;
        $this->expiry = !empty($params[3]) ? $params[3] : 3600;
    }

    public function delete($key, $timeout = 0)
    {
        if (!$this->connect() || empty($key)) {
            return FALSE;
        }

        return $this->memcached->delete($key, $timeout);
    }

    public function get($key, $useCache = TRUE)
    {
        if (!$this->connect() || empty($key)) {
            return NULL;
        } else {
            if ($useCache && $getEpiCache = $this->getEpiCache($key)) {
                return $getEpiCache;
            } else {
                $value = $this->memcached->get($key);
                $this->setEpiCache($key, $value);
                return $value;
            }
        }
    }

    public function set($key = NULL, $value = NULL, $ttl = NULL)
    {
        if (!$this->connect() || empty($key) || $value === NULL) {
            return FALSE;
        }

        $expiry = $ttl === NULL ? $this->expiry : $ttl;
        $this->memcached->set($key, $value, $expiry);
        $this->setEpiCache($key, $value);
        return TRUE;
    }

    private function connect()
    {
        if (self::$connected === TRUE) {
            return TRUE;
        }

        if (class_exists('Memcached')) {
            $this->memcached = new Memcached;

            if ($this->memcached->addServer($this->host, $this->port)) {
                return self::$connected = TRUE;
            } else {
                EpiException::raise(new EpiCacheMemcacheConnectException('Could not connect to memcache server'));
            }
        }

        EpiException::raise(new EpiCacheMemcacheClientDneException('No memcache client exists'));
    }
}

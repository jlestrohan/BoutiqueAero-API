<?php

class EpiSession_Memcached implements EpiSessionInterface
{
    private static $connected = FALSE;
    private $key = NULL;
    private $store = NULL;

    public function __construct($params = array())
    {
        if (empty($_COOKIE[EpiSession::COOKIE])) {
            $cookieVal = md5(uniqid(rand(), TRUE));
            setcookie(EpiSession::COOKIE, $cookieVal, time() + 1209600, '/');
            $_COOKIE[EpiSession::COOKIE] = $cookieVal;
        }
        $this->host = !empty($params[0]) ? $params[0] : 'localhost';
        $this->port = !empty($params[1]) ? $params[1] : 11211;;
        $this->compress = !empty($params[2]) ? $params[2] : 0;;
        $this->expiry = !empty($params[3]) ? $params[3] : 3600;
    }

    public function end()
    {
        if (!$this->connect()) {
            return;
        }

        $this->memcached->delete($this->key);
        $this->store = NULL;
        setcookie(EpiSession::COOKIE, NULL, time() - 86400);
    }

    public function get($key = NULL)
    {
        if (!$this->connect() || empty($key) || !isset($this->store[$key])) {
            return FALSE;
        }

        return $this->store[$key];
    }

    public function getAll()
    {
        if (!$this->connect()) {
            return;
        }

        return $this->memcached->get($this->key);
    }

    public function set($key = NULL, $value = NULL)
    {
        if (!$this->connect() || empty($key)) {
            return FALSE;
        }

        $this->store[$key] = $value;
        $this->memcached->set($this->key, $this->store);
        return $value;
    }

    private function connect($params = NULL)
    {
        if (self::$connected) {
            return TRUE;
        }

        if (class_exists('Memcached')) {
            $this->memcached = new Memcached;
            if ($this->memcached->addServer($this->host, $this->port)) {
                self::$connected = TRUE;
                $this->key       = empty($key) ? $_COOKIE[EpiSession::COOKIE] : $key;
                $this->store     = $this->getAll();
                return TRUE;
            } else {
                EpiException::raise(new EpiSessionMemcacheConnectException('Could not connect to memcache server'));
            }
        }
        EpiException::raise(new EpiSessionMemcacheClientDneException('Could not connect to memcache server'));
    }
}

class EpiSessionMemcacheConnectException extends EpiException
{
}

class EpiSessionMemcacheClientDneException extends EpiException
{
}

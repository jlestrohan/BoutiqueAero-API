<?php

class EpiSession_Apc implements EpiSessionInterface
{
    private $key = NULL;
    private $store = NULL;

    public function end()
    {
        apc_delete($this->key);
        setcookie(EpiSession::COOKIE, NULL, time() - 86400);
    }

    public function get($key = NULL)
    {
        if (empty($key) || !isset($this->store[$key])) {
            return FALSE;
        }

        return $this->store[$key];
    }

    public function getAll()
    {
        return apc_fetch($this->key);
    }

    public function set($key = NULL, $value = NULL)
    {
        if (empty($key)) {
            return FALSE;
        }

        $this->store[$key] = $value;
        apc_store($this->key, $this->store);
        return $value;
    }

    public function __construct($params = NULL)
    {
        if (!empty($params)) {
            $key = array_shift($params);
        }

        if (empty($key) && empty($_COOKIE[EpiSession::COOKIE])) {
            setcookie(EpiSession::COOKIE, md5(uniqid(rand(), TRUE)), time() + 1209600, '/');
        }

        $this->key   = empty($key) ? $_COOKIE[EpiSession::COOKIE] : $key;
        $this->store = $this->getAll();
    }
}

<?php
/**
 *
 *
 * @package    usersClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 */

require_once (__DIR__ . '/objects/userObject.php');
require_once (__DIR__ . '/objects/professionObject.php');
require_once (__DIR__ . '/objects/livraisonAdressObject.php');
require_once (__DIR__ . '/objects/clientCiviliteObject.php');
require_once (__DIR__ . '/models/Livraison.php');
require_once (__DIR__ . '/models/Professions.php');
require_once (__DIR__ . '/models/ClientsCivilites.php');

// ATTENTION AUCUNE SECURITE ENCORE MISE EN PLACE!!

class usersClass
{
    /**
     * section API
     */
    const SECTION = "users";

    /**
     * @return array
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // parametre command obligatoire pour les API users
        if (isset($_POST['command'])) {
            switch ($_POST['command']) {
                case "login":
                    // necessite les parametres LOGIN et PASSWORD
                    if (!isset($_POST['username'])) $returnable->setError(new errorObject(errorObject::ERROR_MISSING_USERNAME));
                    elseif (!isset($_POST['password'])) $returnable->setError(new errorObject(errorObject::ERROR_MISSING_PASSWORD));
                    else $returnable->setData(self::_login($_POST, $returnable->options));
                    break;

                // enregistrement d'une nouvelle adresse de livraison
                case "newShippingAddress":
                    // test si champs required presents et populated
                    if (!isset($_POST['clientId']) or
                        !isset($_POST['livraisonCivilite']) or
                        !isset($_POST['livraisonNom']) or
                        !isset($_POST['livraisonPrenom']) or
                        !isset($_POST['livraisonAdresse1']) or
                        !isset($_POST['livraisonCodePostal']) or
                        !isset($_POST['livraisonVille']) or
                        !isset($_POST['livraisonPays']) or
                        !isset($_POST['livraisonTitre'])) {
                            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
                    } else {
                        $returnable->setData(self::_set_new_shipping_address($post));
                    }

                    break;

                default:
                    // aucun parametre reconnu on renvoie donc une erreur
                    $returnable->setError(new errorObject(errorObject::ERROR_METHOD_NOT_SUPPORTED));
                    break;
            }
        } else {
            // aucun parametre 'command' on renvoie donc une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }
        return $returnable;
    }

    /**
     * @return responseObject
     */
    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // parametre command obligatoire pour les API users
        if (isset($_GET['command'])) {
            switch ($_GET['command']) {
                case "adresses":
                    $returnable->setData(self::_adresses_livraison(filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)));
                    break;

                /**
                 * "civilite_type renvoie le libellé correspondant à l'entier 'type' donné en argument
                 */
                case "civilite":
                    $returnable->setData(self::_client_civilite(filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)));
                    break;

                case "list_professions":
                    $returnable->setData(self::_list_professions($returnable->options));
                    break;

                default:
                    // aucun parametre reconnu on renvoie donc une erreur
                    $returnable->setError(new errorObject(errorObject::ERROR_METHOD_NOT_SUPPORTED));
                    break;
            }
        } else {
            // aucun parametre 'command' on renvoie donc une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }
        return $returnable;
    }

    protected static function _set_new_shipping_address($post)
    {
        // création de l'objet adresse
        $shipping = Livraison::create(array(    'clientId'              => $_POST['clientId'],
                                                'livraisonCivilite'     => $_POST['livraisonCivilite'],
                                                'livraisonNom'          => $_POST['livraisonNom'],
                                                'livraisonPrenom'       => $_POST['livraisonPrenom'],
                                                'livraisonAdresse1'     => $_POST['livraisonAdresse1'],
                                                'livraisonAdresse2'     => $_POST['livraisonAdresse2'],
                                                'livraisonAdresse3'     => $_POST['livraisonAdresse3'],
                                                'livraisonCodePostal'   => $_POST['livraisonCodePostal'],
                                                'livraisonVille'        => $_POST['livraisonVille'],
                                                'livraisonPays'         => $_POST['livraisonPays'],
                                                'livraisonTitre'        => $_POST['livraisonTitre']));
    }

    /**
     * @param $username string
     * @param $password string
     * @return array|errorObject
     */
    protected static function _login($post)
    {
        $conditions = array('   clientEmail = ? AND (clientPassword = PASSWORD(?) OR (clientPassword = OLD_PASSWORD(?) AND clientPassword > ""))', $post['username'], $post['password'], $post['password']);
        try {
            $row = Clients::last(array(
                    'conditions' => $conditions,
                    'order' => 'clientID desc',
                    'limit' => 1
            ));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if ($row instanceof Clients) {
            $userobject = new userObject($row);

            return array("globalcount" => 1, "filteredData" => $userobject);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }


    /**
     * @param $id int id client
     */
    protected static function _adresses_livraison($id)
    {
        try {
            $rows = Livraison::find_all_by_clientid_and_inactive($id,0, array(
                    'order' => 'livraisonID desc'
                ));
        } catch (Exception $e) {
            //var_dump($e);
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }


        if (!empty($rows)) {
            $array = array();
            foreach ($rows as $row) {
                $array[] = new livraisonAdressObject($row);
            }
            return array("globalcount" => 1, "filteredData" => $array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }


    /**
     * @param $id int id client
     */
    protected static function _client_civilite($idCivilite)
    {
        try {
            $row = ClientsCivilites::find($idCivilite);
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        $object = new clientCiviliteObject($row);
        return array("globalcount" => 1, "filteredData" => $object);
    }

    /**
     * @return array|null retourne la liste des professions disponibles (array de models ou null si rien)
     */
    protected static function _list_professions($options)
    {
        $rows = Professions::all(array(
            'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
        ));

        $count = Professions::count();

        if (!empty($rows)) {
            $array = array();
            foreach ($rows as $row) {
                $array[] =  new professionObject($row);
            }
            return array("globalcount" => $count, "filteredData" => $array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * @param $array
     */
    final public static function _finalizeLogin($array)
    {
        // EXTRAIT DU CODE DE MERDE DEPUIS LE SITE, A VERIFIER ET REECRIRE!!

        /*$_SESSION['email']=$_REQUEST['login'];
        $this->assign("LOGOK",1);
        $client = $recordSet->GetArray();
        $this->assign("CLIENT",$client);

        $_SESSION['nom']=$client[0]["clientNom"];
        $_SESSION['prenom']=$client[0]["clientPrenom"];
        $_SESSION['pays']=$client[0]["clientPays"];
        $_SESSION['paysFacturation']=$client[0]["clientPays"];
        $_SESSION['utilisateurID'] = $client[0]["clientID"];
        $_SESSION['clientTVA'] = $client[0]["clientTVA"];
        $_SESSION['clientTVAverif'] = $client[0]["clientTVAverif"];
        $_SESSION['clientListeCadeauxSet'] = (int)$client[0]["clientListeCadeauxSet"];
        $_SESSION['clientListeCadeauxStatut'] = (int)$client[0]["clientListeCadeauxStatut"];

        if ($client[0]["modeEnvoi"] == '0') $_SESSION['modeEnvoi'] = 1;
        elseif ($client[0]["modeEnvoi"] == '1') $_SESSION['modeEnvoi'] = 2;
        else $_SESSION['modeEnvoi'] = 0;

        $_SESSION['authenticated'] = 1;
        $_SESSION['clientID'] = $client[0]["clientID"];


        //on logue la session dans clientSessions
        $rs = "insert into clientSessions (clientID, loginDate, userAgent, remote_addr) values(?,now(),?,?)";
        $recordSet = &$dmwebdb->Execute($rs, Array(1=>$client[0]["clientID"], 2=>$_SERVER['HTTP_USER_AGENT'], 3=>$_SERVER['REMOTE_ADDR']));

        if((int)$_SESSION['prixSpecialID'] == 0 )
             $_SESSION['prixSpecialID'] = (int)$client[0]['prixSpecialID'];

        $_SESSION['clientEnCompte'] = (int)$client[0]['clientEnCompte'];

        if(isset($_SESSION['checkout_step']))
             {
        $_SESSION['authenticated'] = 1;
        $_SESSION['clientID'] = $client[0]["clientID"];
             }

             if(isset($_SESSION['envies_id']))
             {
                  // on r�cup�re les envies
                  $envies_produit = $_SESSION['envies_id'];
                  // maintenant on va transmettre les envies dans la BDD
                  $nb_produits = count($envies_produit);
                  for($i=0;$i<$nb_produits;$i++)
                  {
                  $rs = "SELECT * FROM produits WHERE produits.produitID = ?";
                  $recordSet = &$dmwebdb->Execute($rs, Array(1=>$envies_produit[$i]));
                       if($recordSet->RecordCount() == 0)
                       {

                       // produit invalide
                       // nop ... on passe a la suite
                       }
                       else
                       {
                       // produit valide, on ajoute � la BDD
                       $rs = "INSERT INTO envies (produitID,clientID) VALUES (?,?)";
                       $recordSet = &$dmwebdb->Execute($rs, Array(1=>(int)$envies_produit[$i],2=>(int)$client[0]["clientID"]));

                       }
                  }

             }

             // maintenant on transf�re les envies de la BDD en session (le d�doublonnage se fait automatiquement plus tard)
                  $rs = "SELECT produitID FROM envies WHERE clientID=?";
                  $recordSet = &$dmwebdb->Execute($rs, Array(1=>(int)$client[0]["clientID"]));
                  if($recordSet->RecordCount() > 0)
                       {
                       $produits = $recordSet->GetArray();
                       for($k=0;$k<@count($produits);$k++)
                            $envies_produit[] = $produits[$k]['produitID'];
                       //echo "<!--".print_r($envies_produit)."-->";
                       unset($_SESSION['envies_id']);
                       $_SESSION['envies_id'] = $envies_produit;

                       }



             if(isset($_SESSION['cadeaux_id']))
             {
                  // on r�cup�re les envies
                  $cadeaux_produit = $_SESSION['cadeaux_id'];
                  // maintenant on va transmettre les cadeaux dans la BDD
                  $nb_produits = count($cadeaux_produit);
                  for($i=0;$i<$nb_produits;$i++)
                  {
                  $rs = "SELECT * FROM produits WHERE produits.produitID = ?";
                  $recordSet = &$dmwebdb->Execute($rs, Array(1=>$cadeaux_produit[$i]));
                       if($recordSet->RecordCount() == 0)
                       {

                       // produit invalide
                       // nop ... on passe a la suite
                       }
                       else
                       {
                       // produit valide, on ajoute � la BDD
                       $rs = "INSERT INTO cadeaux (produitID,clientID) VALUES (?,?)";
                       $recordSet = &$dmwebdb->Execute($rs, Array(1=>(int)$cadeaux_produit[$i],2=>(int)$client[0]["clientID"]));

                       }
                  }

             }

             // maintenant on transf�re les cadeaux de la BDD en session (le d�doublonnage se fait automatiquement plus tard)
                  $rs = "SELECT produitID FROM cadeaux WHERE clientID=?";
                  $recordSet = &$dmwebdb->Execute($rs, Array(1=>(int)$client[0]["clientID"]));
                  if($recordSet->RecordCount() > 0)
                       {
                       $produits = $recordSet->GetArray();
                       for($k=0;$k<@count($produits);$k++)
                            $cadeaux_produit[] = $produits[$k]['produitID'];
                       //echo "<!--".print_r($envies_produit)."-->";
                       unset($_SESSION['cadeaux_id']);
                       $_SESSION['cadeaux_id'] = $cadeaux_produit;

                       }


             if((int)$_SESSION['prixSpecialID'] > 0 && !isset($_REQUEST['redirect']) )
             {
                  // si on b�n�ficie de tarifs sp�ciaux, redirection vers la page sp�ciale
                  header('Location: tarifs-speciaux.aero');
                  exit;
             }



             if($_SESSION['authenticated'] == "1" && isset($_REQUEST['redirect']))
             {
                  header('Location: '.$_REQUEST['redirect']);
                  exit;
             }


// ajout� dans le cadre du concours, sera retir� ult�rieurement

             if($_POST['concours'] == "1")
             {
                  header('Location: concours-A380.aero');
                  exit;
             }








        }*/
    }

//
// Dear maintainer:
//
// Once you are done trying to 'optimize' this class,
// and have realized what a terrible mistake that was,
// please increment the following counter as a warning
// to the next guy:
//
// total_hours_wasted_here = 42
//
}

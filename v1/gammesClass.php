<?php
	/**
	 * Objet de réponse routing API pour les produits
	 *
	 * @package   gammesClass.php
	 * @author     Jack Lestrohan
	 * @copyright  2014 boutique.aero
	 * @license    All rights protected
	 * @version    v1
	 * @link       http://www.boutique.aero/api/v1/
	 *
	 */

    require_once (__DIR__ . '/objects/gammeObject.php');
require_once (__DIR__ . '/models/Gammes.php');

	class gammesClass
	{
		/**
		 * section API
		 */
		 const SECTION = "gammes";

		/**
		 * Point d'entrée de la section Gammes
		 * @return array $returnable contenant la reponse JSON
		 */
		public static function apiPost()
		{
            $returnable = new responseObject(self::SECTION);

			// room for post requests eventually
            // par défaut on demande un parametre sinon on colle une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));

            return $returnable;
		}

        /**
         * @return responseObject
         */
        public static function apiGet()
        {
            $returnable = new responseObject(self::SECTION);

            // RECHERCHE PAR ID
            if ($retGetId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
                $returnable->setData(self::getGammeById($retGetId));
            }

            // RECHERCHE PAR NOM
            if ($retGetNameContains = filter_input(INPUT_GET, 'nameContains', FILTER_SANITIZE_STRING)) {
                $returnable->setData(self::getGammeByName($retGetNameContains, $returnable->options));
            }

            // LISTE PAR RAYON
            if ($retGetRayonID = filter_input(INPUT_GET, 'rayonID', FILTER_SANITIZE_NUMBER_INT)) {
                $returnable->setData(self::getGammeListByRayon($retGetRayonID, $returnable->options));
            } else {
                // par défaut on demande un parametre sinon on colle une erreur
                $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
            }

            return $returnable;
        }

		/**
		 * Recherche une gamme a partir de son $id
		 * @param string $id id de la gamme a chercher
		 * @return array $data contenant la reponse JSON
		 */
		private static function getGammeById($id)
		{
            try {
                $rows = Gammes::find($id);
            } catch (\ActiveRecord\RecordNotFound $e) {
                return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
            } catch (Exception $e) {
                return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
            }
            $object = new gammeObject($rows);
            return array("globalcount" => 1, "filteredData" => $object);
		}

		/**
		 * Recherche une gamme a partir d'un pattern texte
		 * @param string $id id de la gamme a chercher
		 * @param array $options tableau d'options a integrer a la reponse
		 * @return array contenant le compte global non filtré 'globalcount' des elements
		 * pouvant être extraits, et la reponse JSON 'filteredData'
		 */
		private static function getGammeByName($name, $options)
		{
            $conditions = array('gammeLibelle LIKE "%'.$name.'%" AND displayOnSite = 0');

            try {
                $rows = Gammes::all(array(
                        'conditions' => $conditions,
                        'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
                    ));
            } catch (Exception $e) {
                return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
            }

            if (!empty($rows)) {
                $gamme_array = array();
                foreach ($rows as $gamme) {
                    $gamme_array[] = new gammeObject($gamme);
                }
                return array("globalcount" => count($rows), "filteredData" => $gamme_array);
            } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
		}

        /**
         * @param $rayonID
         * @param $options
         * @return array|null
         */
        private static function getGammeListByRayon($rayonID, $options)
        {
            try {
                $rows = Gammes::all(array( 'conditions' => array('rayonID = ? AND displayOnSite = 0', $rayonID)));
            } catch (Exception $e) {
                return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
            }

            if (!empty($rows)) {
                $gammes_array = array();
                foreach ($rows as $gamme) {
                    $gammes_array[] = new gammeObject($gamme);
                }
                return array("globalcount" => count($rows), "filteredData" => $gammes_array);
            } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        }
	}
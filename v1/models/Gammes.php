<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 19/08/2014
 * Time: 15:49
 */

/*
    gammeID	                int(10)
	gammeLibelle	        text
	gammeLibelleSite	    text
	gammeDescriptif	        text
	tvaID	                int(10)
	fournisseurID	        int(10)
	rayonID	                int(10)
	displayOnSite	        tinyint(3)
	doControleMagasin	    tinyint(3)
	gammeDateControleStock	datetime
 */

class Gammes extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'gammeID';

    # explicit table name
    static $table_name = 'gammes';
}
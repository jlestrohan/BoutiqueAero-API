<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 31/07/2014
 * Time: 12:24
 */

/*
    marqueID	    int(10)
	marqueLibelle	text
	fournisseurID	int(10)
	DisplayOnSite	tinyint(3)
	isPermanent	    tinyint(3)
*/

class Marques extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'marqueid';
}
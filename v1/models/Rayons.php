<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 01/08/2014
 * Time: 17:29
 */

/*
    rayonID	        int(10)
	rayonLibelle	text
	rayonDescriptif	text
	tvaID	        int(10)
	fournisseurID	int(10)
	displayOnSite	tinyint(3)
*/


class Rayons extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'rayonID';
}
<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 19/08/2014
 * Time: 17:52
 */

/*
    commandeID	        int(10)
	livraisonCivilite	text
	livraisonNom	    text
	livraisonPrenom	    text
	livraisonAdresse1	text
	livraisonAdresse2	text
	livraisonAdresse3	text
	livraisonCodePostal	text
	livraisonVille	    text
	livraisonPays	    text
*/

class CommandesLivraison extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'commandeid';

    # explicit table name
    static $table_name = 'commandesLivraison';

    // validators
    static $validates_presence_of = array(
        array('commandeid'), // commandeid > 1
        array('livraisoncivilite'),
        array('livraisonnom'),
        array('livraisoncodepostal'),
        array('livraisonville'),
        array('livraisonpays'),
    );

    static $validates_numericality_of = array(
        array('commandeid', 'greater_than' => 1), // > 1
        array('commandeid', 'only_integer' => true),
        array('livraisoncivilite', 'only_integer' => true),
    );

    static $validates_size_of = array(
        array('livraisonnom', 'within' => array(1,32), 'too_short' => 'too long!'),
        array('livraisoncodepostal', 'maximum' => 10, 'too_long' => 'should be short and sweet'),
        array('livraisonnom', 'within' => array(1,50), 'too_short' => 'too long!'),
        array('livraisonpays', 'within' => array(1,50), 'too_short' => 'too long!'),
    );
}
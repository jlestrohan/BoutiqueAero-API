<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 01/08/2014
 * Time: 18:08
 */

/*
    produitID	                            int(10)
	produitID_AP	                        int(10)
	fatherID	                            int(10)
	produitLibelle	                        text
	produitLibelleEN	                    text
	produitLibelleMenu	                    text
	gammeID	                                int(10)
	codebar	                                text
	produitDisponibilite	                tinyint(3)
	produitMessage	                        text
	produitMentionPreparation	            text
	produitMentionVente	                    int(1)
	produitMentionEmballage	                text
	produitDisplayOnSite	                tinyint(3)
	produitDisplaySpecial	                text
	produitDesignation	                    text
	produitDesignationTicket	            text
	produitShortDescription	                text
	produitLongDescription	                text
	produitShortDescriptionEN	            text
	produitLongDescriptionEN	            text
	produitBookEditorID	                    int(10)
	produitBookAuthorID	                    int(10)
	produitBookEditorPrice	                float
	produitPrixTTC	                        float
	produitPrixTTC_temp	                    float
	tvaID	                                int(10)
	produitPrixSiteTTC	                    float
	produitPrixSiteTTC_temp	                float
	etat_temp	                            int(1)
	produitType	                            tinyint(3)
	produitPaysOrigine	                    int(10)
	fournisseurID	                        int(10)
	produitNecessiteFacture	                tinyint(4)
	produitLivraisonDelaiMin	            text
	produitLivraisonDelaiMax	            text
	produitFournisseurReference	            text
	produitFournisseurLibelle	            text
	produitFournisseurConditionnement	    text
	produitFournisseurConditionnementOblSug	int(1)
	produitFournisseurQuantiteMin	        text
	produitFournisseurPrix	                float
	produitFournisseurPrix_datemodif	    datetime
	produitFournisseurPrix_temp	            float
	produitFournisseurDevise	            text
	produitCompose	                        tinyint(3)
	produitPrixRevient	                    float
	produitPrixRevient_datemodif	        datetime
	produitStockMaxi	                    int(10)
	produitStockMini	                    int(10)
	produitStockDeclenchement	            int(10)
	produitStockActuel	                    int(11)
	produitStockAuto	                    tinyint(3)
	produitSeuilAlerte	                    int(10)
	produitSeuilAlerteOK	                int(1)
	produitDateControleStock	            datetime
	produitStockage	                        text
	produitPoids	                        int(10)
	produitPointsTransport	                int(10)
	produitDimensionsSpeciales	            tinyint(3)
	dateCreation	                        datetime
	isNew	                                tinyint(3)
	isNewEnd	                            date
	isPromo	                                tinyint(3)
	isTheme	                                tinyint(3)
	isFinSerie	                            tinyint(3)
	isSelection	                            tinyint(3)
	keywords	                            text
	keywordsEN	                            text
	info1Text                           	text
	info1URL	                            text
	info2Text	                            text
	info2URL	                            text
	notes	                                text
	produitJourDate	                        date
	produitJourPrix	                        float
	produitJourLibelle	                    text
	produitJourActive	                    tinyint(3)
	produitJourAncienPrix	                float
	produitEndepot	                        tinyint(3)
	produitValidationTechnique	            varchar(10)
	produitValidationTechniqueDate	        date
	produitValidationLitteraire	            varchar(10)
	produitValidationLitteraireDate	        date
	produitNomenclatureDouane	            text
	isCabAero	                            tinyint(3)
	noBarCode	                            tinyint(3)
	produitStockPrecedent	                int(11)
	produitTableauSpecial	                tinyint(3)
	motscle	                                text
	marqueID	                            int(10)
	produitIndispoDate	                    date
	produitLivAttendueDate	                date
	produitCtrlMagDate	                    datetime
*/

class Produits extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'produitID';
}
<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 31/07/2014
 * Time: 12:24
 */

/*
 *  clientID	                        int(10)
	clientType	                        tinyint(3)
	clientTypePresse	                int(1)
	clientCivilite	                    tinyint(3)
	clientDOB	                        date
	clientNom	                        text
	clientPrenom	                    text
	clientAdresse1	                    text
	clientAdresse2	                    text
	clientAdresse3	                    text
	clientCodePostal	                text
	clientVille	                        text
	clientPays	                        text
	clientContact	                    text
	clientTelephone	                    text
	clientFax	                        text
	clientPortable	                    text
	clientEmail	                        text
	clientTVA	                        text
	clientTVAverif	                    int(1)
	modeReglement	                    tinyint(3)
	modeEnvoi	                        tinyint(4)
	clientFacture	                    tinyint(4)
	clientDerniereCommande	            date
	clientPremiereCommande	            date
	clientDernierContact	            date
	clientPassword	                    text
	clientRemarque	                    text
	clientConsigne	                    text
	clientSite	                        text
	profession	                        text
	interets	                        text
	clientNonAero	                    tinyint(3)
	clientInfosBoutique	                tinyint(3)
	clientInfosBlogs	                tinyint(3)
	clientInfosSpecifiques	            tinyint(3)
	clientInfosAgenda	                tinyint(3)
	clientSiteWebmaster	                text
	clientContactCivilite	            tinyint(3)
	clientContactNom	                text
	clientContactPrenom	                text
	clientContactFonction	            text
	clientContactTelephone	            text
	clientMailingOk	                    tinyint(3)
	clientListeCadeauxSet	            tinyint(3)
	clientListeCadeauxStatut	        tinyint(3)
	clientListeCadeauxAnonymat	        tinyint(3)
	clientListeCadeauxModeAcces	        tinyint(3)
	clientListeCadeauxAdresseLivraison	int(10)
	clientListeCadeauxModeDisplay	    tinyint(3)
	clientListeCadeauxSendMail	        tinyint(3)
	clientEnCompte	                    tinyint(3)
	dateCreation	                    datetime
	isValid	                            tinyint(3)
	blogUID	                            int(10)
	seed	                            text
	prixSpecialID	                    int(10)
	dateModification	                datetime
	clientNonDoublon	                tinyint(3)
	aerodrome                           text
 *
 *
 */


class Clients extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'clientID';
}
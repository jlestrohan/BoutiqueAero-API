<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 22/08/2014
 * Time: 17:48
 */

/*
 *  id	            int(11)
	libelle	        varchar(20)
	abbrev	        varchar(10)
	personneMorale	tinyint(4)
 */

class ClientsCivilites extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'id';

    # explicit table name
    static $table_name = 'clients_civilite';
}
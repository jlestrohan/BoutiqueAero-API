<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 19/08/2014
 * Time: 14:34
 */

/*
 *  commandeID	                    int(10)
	clientID	                    int(10)
	commandeDate	                datetime
	clientIP	                    text
	paiementMode	                tinyint(3)
	port	                        float
	apayer	                        float
	acompte	                        float
	acompteReferences	            text
	commandeLivraison	            int(10)
	commandeLivraisonHasChanged	    tinyint(3)
	commandeStatus	                tinyint(3)
	factureID	                    int(10)
	export	                        tinyint(3)
	commandeColisNumero	            varchar(50)
	commandeColisTransporteur	    tinyint(3)
	commandeExpeditionDate	        date
	commandeCommentaires	        text
	commandeReminderDate	        datetime
	commandeReminderSent	        tinyint(3)
	commandePaiementRecu	        date
	optionCadeau	                tinyint(3)
	optionAssurance	                tinyint(3)
	optionAssuranceAero	            tinyint(3)
	optionFacture	                tinyint(3)
	commandeOfferte	                tinyint(3)
	commandeOfferteOwnerID	        int(10)
	optionMessage	                tinyint(3)
	message	                        text
	commandeOrigine	                tinyint(3)
	devisID	                        int(10)
	codePromo	                    varchar(30)
	debug	                        text
	annulationDate	                datetime
	annulationClient	            tinyint(3)
	expeditionsComplementaires	    text
	chequeCadeauMontant	            float
	chequeCadeauNumero	            text
	commandePaysFacturation	        text
	commandePaysLivraison	        text
	commandeColisNPEC	            varchar(50)
	typeColisID	                    tinyint(3)
	commandeColisStatus	            tinyint(3)
	commandePoids	                float(5,2)
	commandeBrDate	                date
	portAero	                    float(5,3)
	referencesFacture	            text
	remarqueFacture	                text
	remindFromUser	                varchar(50)
	commandeLivraisonDate	        date
	commandeLivraisonStatut	        text
	commandeLivraisonActivityDates	date
	commandeClientEnvoye	        int(1)
	userCommandeEnvoye	            text
	preparationOK	                int(8)
	emballageOK	                    int(8)
	nom_recup	                    text
	date_recup	                    date
	comment_embcadeau	            text
	colisLastStatut	                text
	colisLastStatutSite	            text
	colisLastStatutDate	            varchar(100)
 */

class Commandes extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'commandeid';

    static $before_save = array("setDate");


    public function setDate()
    {
        $this->commandedate = date("Y-m-d H:i:s");
    }

}
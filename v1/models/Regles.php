<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 31/07/2014
 * Time: 12:24
 */

/*
    regleID	        int(10)
	category_id	    int(10)
	regleLibelle	varchar(255)
	topic_content	varchar(7000)
	display_order	int(10)
 */

class Regles extends ActiveRecord\Model
{
    # explicit table name
    static $table_name = 'regles_commerciales';

    # explicit id
    static $primary_key = 'regleid';
}
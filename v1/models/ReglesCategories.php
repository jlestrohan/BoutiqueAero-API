<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 31/07/2014
 * Time: 17:02
 */

/*
    categoryID	                int(11)
	categoryLibelle	            varchar(255)
	categoryLibelleSousTitre	varchar(255)
*/

class ReglesCategories extends ActiveRecord\Model
{
    # explicit table name
    static $table_name = 'regles_commerciales_category';

    # explicit id
    static $primary_key = 'categoryID';
}
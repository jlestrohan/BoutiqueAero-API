<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 01/08/2014
 * Time: 18:47
 */

/*
    tvaID	    int(10)
	intitule	text
	taux	    float
*/

class Tva extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'tvaID';
}
<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 22/08/2014
 * Time: 16:36
 */

/*
    professionID	tinyint(3)
	profession	    text
*/

class Professions extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'professionID';

    # explicit table name
    static $table_name = 'professions';
}
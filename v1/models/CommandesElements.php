<?php

/**
 * Created by PhpStorm.
 * User: jack
 * Date: 19/08/2014
 * Time: 14:34
 */

/*
    elementID	            int(10)
	produitID	            int(10)
	commandeID	            int(10)
	quantite	            int(10)
	commandePrixUnitaire	float
	prepQverified	        int(11)
	prepPverified	        int(11)
	designation	            text
*/

class CommandesElements extends ActiveRecord\Model
{
    # explicit id
    static $primary_key = 'elementid';

    static $table_name = 'commandesElements';

    // VALIDATORS
    static $validates_numericality_of = array(
       array('produitid', 'only_integer' => true),
       array('quantite', array('only_integer' => true, 'greater_than' => 0))
    );
}
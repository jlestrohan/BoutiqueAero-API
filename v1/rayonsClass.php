<?php
/**
 * Objet de réponse routing API pour les rayons
 *
 * @package   rayonsClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 *
 *
 *  Rayons -> gammes -> produits
 */

require_once (__DIR__ . '/objects/rayonObject.php');
require_once (__DIR__ . '/models/Rayons.php');

class rayonsClass
{
    /**
     * section API
     */
    const SECTION = "rayons";

    /**
     * Point d'entrée de la section Rayons
     * @return array $returnable contenant la reponse JSON
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // room for more POST if applicable
        // par défaut on demande un parametre sinon on colle une erreur
        $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));

        return $returnable;
    }

    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // RECHERCHE PAR ID
        if ($retGetId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
            $returnable->setData(self::getRayonById($retGetId));
        }

        // RECHERCHE PAR NOM
        elseif ($retGetNameContains = filter_input(INPUT_GET, 'nameContains', FILTER_SANITIZE_STRING)) {
            $returnable->setData(self::getRayonByName($retGetNameContains, $returnable->options));
        }

        // LIST ALL RAYONS
        elseif (isset($_GET['listRayons'])) {
            $returnable->setData(self::getRayonList($returnable->options));
        } else {
            // aucun parametre 'command' on renvoie donc une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }

        return $returnable;
    }

    /**
     * Recherche un rayon a partir de son $id
     * @param string $id id du rayon a chercher
     * @return array $data contenant la reponse JSON
     * @todo bug si rayonID = 1 la liste des gammes renvoie une série de false. Enquêter
     */
    private static function getRayonById($id)
    {
        try {
            $data = Rayons::find($id);
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        $rayon_object = new rayonObject($data);
        return array("globalcount" => 1, "filteredData" => $rayon_object);
    }

    /**
     * Recherche un rayon a partir d'un pattern texte
     * @param string $id id de la gamme a chercher
     * @param array $options tableau d'options a integrer a la reponse
     * @return array contenant le compte global non filtré 'globalcount' des elements
     * pouvant être extraits, et la reponse JSON 'filteredData'
     */
    private static function getRayonByName($name, $options)
    {
        $rows = Rayons::all(array(
                'conditions' => array('displayonsite = ? AND rayonLibelle LIKE CONCAT("%", ? ,"%")', true, $name),
                'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
            )
        );

        $count = Rayons::count(array(
                'conditions' => array('displayonsite = ? AND `rayonLibelle` LIKE CONCAT("%", ? ,"%")', true, $name)
            )
        );

        if (!empty($rows)) {
            $rayons_array = array();
            foreach ($rows as $rayon) {
                $rayons_array[] = new rayonObject($rayon);
            }
            return array("globalcount" => $count, "filteredData" => $rayons_array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * @param $options
     * @return array|errorObject
     */
    private static function getRayonList($options)
    {
        $conditions = array('displayOnSite = ?', true);
        $rows = Rayons::all(array(
            'conditions' => $conditions,
            'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
        ));

        $count = Rayons::count(array('conditions' => $conditions));

        if (!empty($rows)) {
            $array = array();
            foreach ($rows as $row) {
                $array[] =  new rayonObject($row);
            }
            return array("globalcount" => $count, "filteredData" => $array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }
}

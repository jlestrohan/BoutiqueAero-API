<?php
/**
 * Objet de réponse routing API pour les produits
 *
 * @package   produitsClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 * @uses       motherApi.class
 *
 */

require_once (__DIR__ . '/objects/produitObject.php');
require_once (__DIR__ . '/models/Produits.php');

class produitsClass
{
    const PRODUCT_ALL_FIELDS               = 0;
    const PRODUCT_LONG_DESCRIPTION         = 1;


    /**
     * section API
     */
    const SECTION = "produits";


    /**
     * Point d'entrée de la section Produits
     * @return array $returnable contenant la reponse JSON
     *
     * arguments api produits:
     *   - id= recherche produit par son id
     *   - nameContains= recherche produit par une partie du nom
     *   - produitJour (flag) si indiqué renvoie le produit du jour
     *
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // RDY for POST requests here eventually

        // aucun parametre 'command' on renvoie donc une erreur
        $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));

        return $returnable;
    }

    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // PRODUITS PAR ID - id= (populated)
        if ($retGetId = (int)filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
            // si le flag "longDescription est mis en plus alors on ajoute l'option
            if (isset($_GET['longDescription'])) {
                $idOption = self::PRODUCT_LONG_DESCRIPTION;
            } else {
                $idOption = self::PRODUCT_ALL_FIELDS;
            }
            $returnable->setData(self::getProductById($retGetId, $idOption));

        // PRODUIT DU JOUR - produitJour (flag)
        } elseif (isset($_GET['produitJour'])) {
            $returnable->setData(self::getProduitDuJour());

        // PRODUITS AU HASARD - produitsHasard (flag) produits au hasard
        } elseif (isset($_GET['produitsHasard'])) {
            $returnable->setData(self::getProduitsHasard($returnable->options));

            // PRODUITS PAR FILTRE SUR LE NOM - nameContains= (populated)
        } elseif ($retGetNameContains = filter_input(INPUT_GET, 'nameContains', FILTER_SANITIZE_STRING)) {
                $returnable->setData(self::getProductByName($retGetNameContains, $returnable->options)) ;

        // PRODUITS PAR GAMME - gammeId= (populated)
        } elseif ($retGetGammeId = filter_input(INPUT_GET, 'gammeId', FILTER_SANITIZE_NUMBER_INT)) {
                $returnable->setData(self::getProductByGammeId($retGetGammeId, $returnable->options));


        // PRODUITS FIN DE SERIE - produitsFinSerie (flag) renvoie la liste des produits de fin de série
        } elseif (isset($_GET['produitsFinSerie'])) {
            $returnable->setData(self::getProduitsFinSerie($returnable->options));
        } else {
            // aucun parametre reconnu, on throw une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }

        return $returnable;
    }

    /**
     * Retourne une array
     * @param string $id id de la gamme a chercher
     * @return array $data contenant la reponse JSON
     */
    private static function getProductById($id, $idOption = self::PRODUCT_ALL_FIELDS)
    {
        $join = 'LEFT JOIN tva ON produits.tvaID=tva.tvaID';
        $sel = 'produits.*, tva.*'; // clause select ajoutée manuellement sur joins pour pallier au manque de relations (peut pas utiliser de static belongs_to en modèle :( )

        try {
            $row = Produits::find($id, array(
                'joins' => $join,
                'select'=>$sel
                )
            );
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        $object = new produitObject($row);
        switch ($idOption) {
            case self::PRODUCT_ALL_FIELDS:
                $toreturn = $object;
                break;
            case self::PRODUCT_LONG_DESCRIPTION:
                $toreturn = $object->produitLongDescription;
                break;
        }
        return array("globalcount" => 1, "filteredData" => $toreturn);
    }

    /**
     * Retourne une array contenant le compte total du résultat de la requête (sans
     * LIMIT) ainsi que les données filtrées
     * @param string $name chaine de caracteres sur laquelle baser le like de la
     * requête
     * @param array $options array d'options
     * @return array $data contenant la reponse JSON
     */
    private static function getProductByName($name, $options)
    {
        $join = 'LEFT JOIN tva ON produits.tvaID=tva.tvaID';
        $sel = 'produits.*, tva.*'; // clause select ajoutée manuellement sur joins pour pallier au manque de relations (peut pas utiliser de static belongs_to en modèle :( )

        try {
            $rows = Produits::all(array(
                    'conditions' => array('produitLibelle LIKE "%'.$name.'%"'),
                    'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage'],
                    'joins' => $join,
                    'select'=>$sel
                )
            );
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $count = Produits::count(array('conditions' => array('produitLibelle LIKE "%'.$name.'%"')));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if (!empty($rows)) {
            // iteration $data pour créer un objet commande par enregistrement
            $produits_array = array();
            foreach ($rows as $produit) {
                $produits_array[] = new produitObject($produit);
            }
            return array("globalcount" => $count, "filteredData" => $produits_array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * Retourne une array contenant le produit du jour
     * @return array $produit contenant l'array du produit du jour
     */
    private static function getProduitDuJour()
    {
        $conditions = array('   produitJourActive = 1
                                AND (produits.produitDisponibilite = ? OR produits.produitDisponibilite = ?)',
                                    produitObject::PRODUCT_AVAILABILITY_DISPONIBLE, produitObject::PRODUCT_AVAILABILITY_SUR_COMMANDE);

        $join = 'LEFT JOIN tva ON produits.tvaID=tva.tvaID';
        $sel = 'produits.*, tva.*'; // clause select ajoutée manuellement sur joins pour pallier au manque de relations (peut pas utiliser de static belongs_to en modèle :( )

        try {
            $rows = Produits::all(array(
                    'conditions' => $conditions,
                    'joins' => $join,
                    'select'=>$sel
                ));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $count = Produits::count(array('conditions' => $conditions));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if (!empty($rows)) {
            $object = new produitObject(($rows[0]));
            return array("globalcount" => $count, "filteredData" => $object);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }


    /**
     * Retourne une array contenant 10 produits tirés au hasard (front boutique)
     * @return array $data contenant la reponse JSON
     */
    private static function getProduitsHasard($options)
    {
        $conditions = array(' fatherID = 0 AND isSelection = 1 AND (produits.produitDisponibilite = ? OR produits.produitDisponibilite = ?)',
                                                produitObject::PRODUCT_AVAILABILITY_DISPONIBLE, produitObject::PRODUCT_AVAILABILITY_SUR_COMMANDE);

        $join = 'LEFT JOIN tva ON produits.tvaID=tva.tvaID';
        $sel = 'produits.*, tva.*'; // clause select ajoutée manuellement sur joins pour pallier au manque de relations (peut pas utiliser de static belongs_to en modèle :( )

        try {
            $rows = Produits::all(array(
                    'conditions' => $conditions,
                    'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage'],
                    'joins' => $join,
                    'select'=> $sel
            ));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $count = Produits::count(array('conditions' => $conditions));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if (!empty($rows)) {
            $produits_array = array();
            foreach ($rows as $produit) {
                $produits_array[] = new produitObject($produit);
            }
            return array("globalcount" => $count, "filteredData" => $produits_array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * Retourne une array contenant la liste des produits de fin de série
     * @param array $options tableau d'options
     * @return array $data contenant la reponse JSON
     */
    private static function getProduitsFinSerie($options)
    {
        $conditions = array(' isFinSerie = 1 AND (produits.produitDisponibilite = ? OR produits.produitDisponibilite = ?)',
            produitObject::PRODUCT_AVAILABILITY_DISPONIBLE, produitObject::PRODUCT_AVAILABILITY_SUR_COMMANDE);

        $join = 'LEFT JOIN tva ON produits.tvaID=tva.tvaID';
        $sel = 'produits.*, tva.*'; // clause select ajoutée manuellement sur joins pour pallier au manque de relations (peut pas utiliser de static belongs_to en modèle :( )

        try {
            $rows = Produits::all(array(
                    'conditions' => $conditions,
                    'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage'],
                    'joins' => $join,
                    'select'=> $sel
            ));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $count = Produits::count(array('conditions' => $conditions));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if (!empty($rows)) {
            $produits_array = array();
            foreach ($rows as $produit) {
                $produits_array[] = new produitObject($produit);
            }
            return array("globalcount" => $count, "filteredData" => $produits_array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * Retourne une array contenant le compte total du résultat de la requête (sans
     * LIMIT) ainsi que les données filtrées
     * @param string $name chaine de caracteres sur laquelle baser le like de la
     * requête
     * @param array $options array d'options
     * @return array $data contenant la reponse JSON
     */
    private static function getProductByGammeId($id, $options)
    {
        $join = 'LEFT JOIN tva ON produits.tvaID=tva.tvaID';
        $sel = 'produits.*, tva.*'; // clause select ajoutée manuellement sur joins pour pallier au manque de relations (peut pas utiliser de static belongs_to en modèle :( )
        $conditions = array(' gammeID = ? AND (produits.produitDisponibilite = ? OR produits.produitDisponibilite = ?)', $id, produitObject::PRODUCT_AVAILABILITY_DISPONIBLE, produitObject::PRODUCT_AVAILABILITY_SUR_COMMANDE);

        try {
            $rows = Produits::all(array(
                        'conditions' => $conditions,
                        'joins' => $join,
                        'select'=>$sel
                ));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $count = Produits::count(array('conditions' => $conditions));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if (!empty($rows)) {
            $produits_array = array();
            foreach ($rows as $produit) {
                $produits_array[] = new produitObject($produit);
            }
            return array("globalcount" => $count, "filteredData" => $produits_array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }
}

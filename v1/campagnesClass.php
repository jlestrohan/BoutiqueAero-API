<?php
/**
 * Objet de réponse routing API pour les campagnes de pub page accueil
 *
 * @package    campagnesClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

class campagnesClass
{
    /**
     * section API
     */
    const SECTION = "campagnes";

    /**
     * Point d'entrée de la section Commandes
     * @return array $returnable contenant la reponse JSON
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // par défaut on demande un parametre sinon on colle une erreur
        // si aucune requete post valide, action non authorized
        $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));
        return $returnable;
    }

    /**
     * @return responseObject
     */
    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // par défaut on demande un parametre sinon on colle une erreur
        $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));

        return $returnable;
    }
}

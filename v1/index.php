<?php
/**
 * index doc API 
 * 
 * @package   index.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

require_once (__DIR__ . '/src/Epi.php');

require_once 'php-activerecord/ActiveRecord.php';

// api topics class includes
require_once (__DIR__ . '/produitsClass.php');
require_once (__DIR__ . '/gammesClass.php');
require_once (__DIR__ . '/rayonsClass.php');
require_once (__DIR__ . '/usersClass.php');
require_once (__DIR__ . '/commandesClass.php');
require_once (__DIR__ . '/campagnesClass.php');
require_once (__DIR__ . '/marquesClass.php');
require_once (__DIR__ . '/reglesClass.php');

Epi::setPath('base', 'src');
Epi::setSetting('exceptions', true);

// initialise la configuration database adapter
$dbhost = 'localhost';
$dbuser = 'aero';
$dbpass = 'DcbZWYcpMCF3tHvD';
$dbname = 'aero_dev';
Epi::init('database');
EpiDatabase::employ('mysql', $dbname, $dbhost, $dbuser, $dbpass);

ActiveRecord\Config::initialize(function($cfg)
{
     $cfg->set_model_directory(__DIR__ . '/models');
     $cfg->set_connections(array(
         'development' => 'mysql://aero_dev:DcbZWYcpMCF3tHvD@localhost/aero_dev?charset=utf8'));
});


// initialise le module api de Epiphany
Epi::init('api');

// produits
getApi()->post('/produits.json', array('produitsClass', 'apiPost'), EpiApi::external);
getApi()->get('/produits.json', array('produitsClass', 'apiGet'), EpiApi::external);

// marques
getApi()->post('/marques.json', array('marquesClass', 'apiPost'), EpiApi::external);
getApi()->get('/marques.json', array('marquesClass', 'apiGet'), EpiApi::external);

// gammes
getApi()->post('/gammes.json', array('gammesClass', 'apiPost'), EpiApi::external);
getApi()->get('/gammes.json', array('gammesClass', 'apiGet'), EpiApi::external);

// rayons
getApi()->post('/rayons.json', array('rayonsClass', 'apiPost'), EpiApi::external);
getApi()->get('/rayons.json', array('rayonsClass', 'apiGet'), EpiApi::external);

// users
getApi()->post('/users.json', array('usersClass', 'apiPost'), EpiApi::external);
getApi()->get('/users.json', array('usersClass', 'apiGet'), EpiApi::external);

// commandes
getApi()->post('/commandes.json', array('commandesClass', 'apiPost'), EpiApi::external);
getApi()->get('/commandes.json', array('commandesClass', 'apiGet'), EpiApi::external);

// campagnes pub sur page accueil
getApi()->post('/campagnes.json', array('campagnesClass', 'apiPost'), EpiApi::external);
getApi()->get('/campagnes.json', array('campagnesClass', 'apiGet'), EpiApi::external);

// regles
getApi()->post('/regles.json', array('reglesClass', 'apiPost'), EpiApi::external);
getApi()->get('/regles.json', array('reglesClass', 'apiGet'), EpiApi::external);

getRoute()->run();

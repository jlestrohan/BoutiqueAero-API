<?php
/**
 * Objet de réponse routing API pour les produits
 *
 * @package   gammesClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */
// http://www.phpactiverecord.org/projects/main/wiki/Finders

require_once (__DIR__ . '/objects/marqueObject.php');
require_once (__DIR__ . '/models/Marques.php');

class marquesClass
{
    /**
     * section API
     */
    const SECTION = "marques";


    /**
     * Point d'entrée de la section Marques
     * @return array $returnable contenant la reponse JSON
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // room for more POST requests eventually
        // par défaut on demande un parametre sinon on colle une erreur
        $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));

        return $returnable;
    }

    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // RECHERCHE PAR ID
        if ($retGetId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
            $returnable->setData(self::getMarqueById($retGetId));
        }

        // RECHERCHE PAR NOM
        if ($retGetNameContains = filter_input(INPUT_GET, 'nameContains', FILTER_SANITIZE_STRING)) {
            $returnable->setData(self::getMarqueByName($retGetNameContains, $returnable->options));
        }

        // LIST ALL RAYONS
        elseif (isset($_GET['listMarques'])) {
            $returnable->setData(self::getMarquesList($returnable->options));
        } else {
            // par défaut on demande un parametre sinon on colle une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }

        return $returnable;
    }

    /**
     * Recherche une marque a partir de son $id
     * @param string $id id de la gamme a chercher
     * @return array $data contenant la reponse JSON
     */
    private static function getMarqueById($id)
    {
        try {
            $data = Marques::find((int)$id);
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        $marque_object = new marqueObject($data);
        $options['total'] = isset($marque_object) ? 1 : 0;
        return array("globalcount" => $options['total'], "filteredData" => $marque_object);
    }

    /**
     * Recherche une gamme a partir d'un pattern texte
     * @param string $id id de la gamme a chercher
     * @param array $options tableau d'options a integrer a la reponse
     * @return array contenant le compte global non filtré 'globalcount' des elements
     * pouvant être extraits, et la reponse JSON 'filteredData'
     */
    private static function getMarqueByName($name, $options)
    {
        $rows = Marques::all(array(
                'conditions' => array('displayonsite = ? AND `marqueLibelle` LIKE CONCAT("%", ? ,"%")', true, $name),
                'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
            )
        );

        $count = Marques::count(array(
                'conditions' => array('displayonsite = ? AND `marqueLibelle` LIKE CONCAT("%", ? ,"%")', true, $name)
            )
        );

        // iteration $data pour créer un objet commande par enregistrement
        $marques_array = array();
        foreach ($rows as $marque) {
            $marques_array[] = new marqueObject($marque);
        }
        if (count($marques_array) > 0) return array("globalcount" => $count, "filteredData" => $marques_array);
        else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * Liste toutes les marques
     * @param $options
     * @return array|null
     */
    private static function getMarquesList($options)
    {
        $rows = Marques::all(array(
                'conditions' => array('displayonsite = ?', true),
                'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
            )
        );

        $count = Marques::count(array(
            'conditions' => array(
                'displayonsite = ?', true)
            )
        );

        // iteration $data pour créer un objet commande par enregistrement
        $marques_array = array();
        foreach ($rows as $marque) {
            $marques_array[] = new marqueObject($marque);
        }
        if (count($marques_array) > 0) return array("globalcount" => $count, "filteredData" => $marques_array);
        else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }
}

<?php
/**
 /*
 *
 * Errors object aimed to supply json requests with common error codes
 *
 * @package   errorsObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

class errorObject
{
    /**
     *
     */
    const SUCCESS_REQUEST_COMPLETED         = 0;
    const ERROR_MISSING_ACCESS_TOKEN       	= 1001;
	const ERROR_INVALID_ACCESS_TOKEN       	= 1002;
	const ERROR_ACCESS_TOKEN_EXPIRED       	= 1003;
	const ERROR_ACTION_NOT_AUTHORIZED      	= 1004;
	const ERROR_VERSION_NOT_SUPPORTED      	= 1005;
	const ERROR_UNABLE_TO_PARSE_REQUEST    	= 1006;
	const ERROR_REQUIRED_PARAMETER_MISSING 	= 1007;
	const ERROR_METHOD_NOT_SUPPORTED  		= 1008;
	const ERROR_HEADER_MISSING_OR_INVALID  	= 1009;
	const ERROR_UNKNOWN_ERROR		   	    = 1010;
    const ERROR_METHOD_NOT_IMPLEMENTED      = 1011;
    const ERROR_INVALID_PARAMETER_TYPE      = 1012;
    const ERROR_INVALID_PARAMETER_VALUE     = 1013;
	const ERROR_RECORD_NOT_FOUND 			= 1020;

     // users api specifics
	const ERROR_INVALID_CREDENTIALS        = 1050;
    const ERROR_INACTIVE_ACCOUNT           = 1051;
    const ERROR_MISSING_USERNAME           = 1052;
    const ERROR_INVALID_EMAIL_FORMAT       = 1053;
    const ERROR_MISSING_PASSWORD           = 1054;

    // commandes API specifics
    const ERROR_ORDER_NO_ARTICLE           = 2001;

    // commande_elements API specific
    const ERROR_COMMANDE_ELEMENTS_ZERO_ID       = 2100;
    const ERROR_COMMANDE_ELEMENT_ID_NOT_SET     = 2101;
    const ERROR_COMMANDE_ELEMENT_ZERO_QUANT     = 2102;


    /**
     * @var int
     */
    public $code = 0;

    /**
     * @var string
     */
    public $msg = "";

    /**
     * @param $errorCode
     */
    function __construct($errorCode)
	{
		$this -> code = $errorCode;
		$this -> msg = $this -> _getErrorMsgArray($errorCode);
	}

    /**
     * @param $errorCode
     * @return string
     */
    private function _getErrorMsgArray($errorCode)
	{
		$msg = '';
		switch ($errorCode) {
            // success error labels
            case self::SUCCESS_REQUEST_COMPLETED:
                $msg = 'Request completed succesfully';
                break;
			// API access errors
			case self::ERROR_MISSING_ACCESS_TOKEN :
				$msg = 'Missing Access Token.';
				break;
			case self::ERROR_INVALID_ACCESS_TOKEN :
				$msg = 'Invalid Access Token.';
				break;
			case self::ERROR_ACCESS_TOKEN_EXPIRED :
				$msg = 'Access Token has expired.';
				break;
			case self::ERROR_ACTION_NOT_AUTHORIZED :
				$msg = 'You are not authorized to perform this action.';
				break;
			case self::ERROR_VERSION_NOT_SUPPORTED :
				$msg = 'Version not supported.';
				break;
			case self::ERROR_UNABLE_TO_PARSE_REQUEST :
				$msg = 'Unable to parse request.';
				break;
			case self::ERROR_REQUIRED_PARAMETER_MISSING :
				$msg = 'A required parameter is missing from your request:';
				break;
			case self::ERROR_METHOD_NOT_SUPPORTED :
				$msg = 'HTTP Method not supported.';
				break;
			case self::ERROR_HEADER_MISSING_OR_INVALID :
				$msg = 'A required header was missing or invalid:.';
				break;
            case self::ERROR_INVALID_PARAMETER_TYPE :
                $msg = 'Parameter type mismatch';
                break;
            case self::ERROR_INVALID_PARAMETER_VALUE :
                $msg = 'Missing value for parameter';
                break;

			// login handling errors
			case self::ERROR_INVALID_CREDENTIALS :
				$msg = 'Wrong credentials!';
				break;
               case self::ERROR_INACTIVE_ACCOUNT :
                    $msg = 'Account disabled or inactive!';
                    break;
               case self::ERROR_MISSING_USERNAME :
                    $msg = 'Missing username';
                    break;
               case self::ERROR_MISSING_PASSWORD :
                    $msg = 'Missing password';
                    break;
            case self::ERROR_INVALID_EMAIL_FORMAT :
                    $msg = 'Invalid email format';
                    break;

            // order requests results errors
            case self::ERROR_ORDER_NO_ARTICLE :
                    $msg = 'Orders elements cannot be empty';
                    break;

            // order_element object validation errors
            case self::ERROR_COMMANDE_ELEMENTS_ZERO_ID :
                    $msg = 'Element id cannot equal zero';
                    break;
            case self::ERROR_COMMANDE_ELEMENT_ID_NOT_SET:
                    $msg = 'Element id is not set';
                    break;
            case self::ERROR_COMMANDE_ELEMENT_ZERO_QUANT:
                    $msg = 'Quantity cannot equal zero';
                    break;


			// DB request results errors
			case self::ERROR_RECORD_NOT_FOUND :
				$msg = 'Not Found';
				break;
				
			// Miscelaneous error types
			case self::ERROR_UNKNOWN_ERROR:
				$msg = 'Unknown error';
				break;

            case self::ERROR_METHOD_NOT_IMPLEMENTED:
                $msg = 'Method not yet implemented';
                break;
		}
		return $msg;
	}

}

// bon courage à celui ou celle qui reprendra le flambeau, que ce havre de paix programmatique que sont ces API vous soient bénefiques face au grand n'importe nawok de code dans lequel le reste des sites sont codés.
// De toute mon humble expérience informatique je n'ai personnellement jamais vu un tel bordel, les mecs ont du être torturés pour coder aussi mal... Même en Basic et bourré on ne pourrait faire pire
// Tout est à jeter..
// Que ces API servent un jour comme cela était prévu dès le départ à du développement sur mobile... faute de volonté réelle la direction a préferé stopper net tout investissement, à commencer par un MAC correct et
// quelques devices un peu plus modernes que des iPhone 4.
//
// Le rêve cette boite....
<?php
/**
/*
 *
 * User object
 *
 * @package    clientCiviliteObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

class clientCiviliteObject
{
    /**
     * Id de civilite
     * @var int id id de civilite
     */
    public $id;

    /**
     * libelle
     * @string libelle civilite
     */
    public $libelle;

    /**
     * libelle
     * @string abbrev abbreviation civilite
     */
    public $abbrev;

    /**
     * libelle
     * @bool est-ce une personne morale ?
     */
    public $personneMorale;


    /**
     * @param $record
     */
    public function __construct($record)
    {
        if (!$record instanceof ClientsCivilites)
            throw new Exception('Construction de l\'objet impossible');
        else {
            // for cast purposes...
            $this -> id = (int)$record->id;
            $this -> personneMorale = (bool)$record->personnemorale;
            $this -> libelle = utf8_encode($record->libelle);
            $this -> abbrev = utf8_encode($record->abbrev);
        }
    }
}

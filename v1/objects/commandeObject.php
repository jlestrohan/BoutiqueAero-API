<?php
/**
 /*
 *
 * Commandes object holds a single command
 *
 * @package    commandeObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

require_once ('produitObject.php');
require_once ('commandeElementsObject.php');
require_once (dirname(__FILE__) . '/../produitsClass.php');
require_once (dirname(__FILE__) . '/../models/CommandesElements.php');

 class commandeObject
 {
 		/**
		 * Champs extraits de la table MySQL
		 * Elements de Facture/Commande
		 * @static const ELEMENTS_VALID_FIELDS
		 */
		const ELEMENTS_VALID_FIELDS = "    commandesElements.produitID, 
                                             commandesElements.commandeID,
                                             commandesElements.commandePrixUnitaire,
                                             commandesElements.quantite";
	
 		/**
		 *  Constantes Modes de paiement (en attendant un vrai objet commande)
		 * @static
		 */
		const COMMANDES_MODE_PAIEMENT_ESPECES 		= 0;
		const COMMANDES_MODE_PAIEMENT_CHEQUE 		= 1;
		const COMMANDES_MODE_PAIEMENT_CB_INTERNET 	= 2;
		const COMMANDES_MODE_PAIEMENT_CB 			= 3;
		const COMMANDES_MODE_PAIEMENT_VIREMENT 		= 4;
		const COMMANDES_MODE_PAIEMENT_AVOIR 		= 5;
		const COMMANDES_MODE_PAIEMENT_CB_TELEPHONE 	= 6;
		const COMMANDES_MODE_PAIEMENT_EN_COMPTE		= 99;
		const COMMANDES_MODE_PAIEMENT_CHEQUE_CADEAU 	= 97;
		
		/**
		 * Numéro de commande
		 * @var int $idCommande id de la commande
		 */
		public $idCommande;
		
		/**
		 * id de client
		 * @var int $idClient id du client
		 */
		public $idClient;
		
		/**
		 * Date de commande
		 * @var int $idCommande id de la commande
		 */
		public $commandeDate;
		
		/**
		 * Frais de port
		 * @var string $fraisPort frais de port
		 */
		public $fraisPort;
		
		/**
		 * Montant TTC
		 * @var string $montantTTC Montant total de la commande
		 */
		public $montantTTC;

		/**
		 * Facture id
		 * @var int $factureID identifiant de la facture
		 */
		public $factureID;
		
		/**
		 * Date du paiement
		 * @var string $datePayment date du paiement
		 */
		public $datePaiment;
		
		/**
		 * Mode de paiement
		 * @var string $modePaiement mode de paiement
		 */
		public $modePaiement;
		
		/**
		 * Nombre d'élements de commande
		 * @var array $commandeElementsNb nombre d'articles de cette commande
		 */
		public $commandeElementsNb;
		
		/**
		 * Elements de commande
		 * @array $commandeElements articles de la commande
		 */
		public $commandeElements;
		
		/**
		 * __constructeur 
		 * @param array $data enregistrement issu de MySQL sous forme d'array associative
		 */
		 public function __construct($record) 
		 {
		 	if (empty($record))
                throw new Exception('Construction de l\'objet impossible');
			else {
				$this->idCommande = (int)$record->commandeid;
				$this->commandeDate = $record->commandedate;
				$this->idClient = (int)$record->clientid;
				$this->fraisPort = (float)$record->port;
				$this->montantTTC = (float)$record->apayer;
				$this->factureID = (int)$record->factureid;
				$this->datePaiment = $record->commandepaiementrecu;
				$this->modePaiement = $this->_getPaymentMode($record->paiementmode);

				// populate the object with commandes elements
				$this->_populatesCommandeElements();

			}
		 }

   		/**
		 * Récupère les éléments associés à la commande en cours
         * * private _getCommandeElements
		 */
   		private function _populatesCommandeElements()
   		{
            $rows = CommandesElements::all(array(
                    'conditions' => array('commandeID = ?', $this->idCommande),
                )
            );

            $elements_array = array();

            foreach ($rows as $commandeElement) {
                $elements_object = new commandeElementsObject($commandeElement);
                $elements_array[] = $elements_object;
            }

            $this -> commandeElements = $elements_array;
            $this -> commandeElementsNb = count($elements_array);

   		}


		/**
		 * private _getPaymentMode
		 * @param int $payMode mode de paiment numérique
		 * @return string $paymentMode mode de paiement en toutes lettres
		 */
		 private function _getPaymentMode($payMode)
		 {
		 	switch ((int)$payMode) {
				case self::COMMANDES_MODE_PAIEMENT_ESPECES: return "espèces"; break;
				case self::COMMANDES_MODE_PAIEMENT_CHEQUE: return "chèque"; break;
				case self::COMMANDES_MODE_PAIEMENT_CB_INTERNET:
                case self::COMMANDES_MODE_PAIEMENT_CB:
				case self::COMMANDES_MODE_PAIEMENT_CB_TELEPHONE: return "cb"; break;
				case self::COMMANDES_MODE_PAIEMENT_VIREMENT: return "virement"; break;
				case self::COMMANDES_MODE_PAIEMENT_AVOIR: return "avoir"; break;
				case self::COMMANDES_MODE_PAIEMENT_EN_COMPTE: return "en compte"; break;
				case self::COMMANDES_MODE_PAIEMENT_CHEQUE_CADEAU: return "chèque cadeau"; break;
		 	}
		 }
 }

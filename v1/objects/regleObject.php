<?php
/**
/*
 *
 * R�gle object
 *
 * @package    regleObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */


class regleObject
{
    /**
     * Constantes d�crivant les diff�rentes cat�gories de regles possibles
     * @static const PRODUCT_AVAILABILITY_
     */
    //const REGLE_CATEGORY_CGV = 1;
    //const REGLE CATEGORY_LIVRAISON = 2;

    /**
     * Id de regle
     * @var int $regleID id de la regle
     */
    public $regleid;

    /**
     * Cat�gory ID
     * @var int Indice de la cat�gorie de regle
     */
    public $category_id;

    /**
     * Libell� de la regle (titre)
     * @var string $regleLibelle libell� de la regle
     */
    public $reglelibelle;

    /**
     * Contenu du topic
     * @var string $topic_content
     */
    public $topic_content;

    /**
     * Ordre d'affichage dans la categorie concern�e
     * @var int $display_order
     */
    public $display_order;


    /**
     * @param $record
     */
    public function __construct($record)
    {
        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            // routines de nettoyage au cas ou les regles soient �crites en html dans la base
            $topic = preg_replace('/<[^>]*>/', ' ', utf8_encode($record->topic_content)); // strip tous les tags
            $topic = trim(preg_replace('!\s+!', ' ', $topic)); // strip les multispaces en un seul
            $this -> topic_content = preg_replace('(&nbsp;)', '', $topic); // strip les &nbsp;

            $this -> regleid = (int)$record->regleid;
            $this -> category_id = (int)$record->category_id;
            $this -> display_order = (int)$record->display_order;
        }
    }
}


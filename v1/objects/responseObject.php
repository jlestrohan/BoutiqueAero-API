<?php
/**
 * construit un objet reponse général contenant un header de response ainsi qu'un champ de collection de données 'data'
 * 
 * @package   responseObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

include_once('errorsObject.php');

class responseObject 
{
    /**
     * Jeton client
     */
    const CLIENT_TOKEN_VALUE = 33534605604;

    //private static $APC_cache;

    // test APC cache ?
    // if (extension_loaded('apc')) self::$APC_cache = true;

    /**
     * Constante nombre par défaut d'enregistrements présentés par page
     */
    const DEFAULT_ITEMS_PER_PAGE = 20;

    /**
     * Constante numéro de version de cette API
     */
    const API_VERSION = 0.13;

    /**
     * @var
     */
    public $status;

    /**
     * @var
     */
    public $response;

    /**
     * @var
     */
    public $timestamp;

    /**
     * @var
     */
    public $options;

    /**
     * @param $name
     */
    function __construct($name)
    {
        $this->options = $this->_getOptionArray();

        $this->response = $this->options;
        $this->response['section'] = $name;
        $this->timestamp = $this->_getDateTimeStuffArray();

        $valid = $this->_validate_options($this->options);
        if ($valid instanceof errorObject) {
            $this->setError($valid);
        }
    }

    /**
     * @return array
     */
    private function _getDateTimeStuffArray()
    {
        $tmpArray = array();

        $date = new DateTime('now', new DateTimeZone('Europe/Paris'));

        $tmpArray['date'] = $date->format('d-m-Y');
        $tmpArray['hour'] = $date->format('H:i:s');
        $tmpArray['timestamp'] = $date->getTimestamp();

        return $tmpArray;
    }

    /**
     * @param $data
     */
    public function setData($data)
    {
        $validate = $this->_validate_options($this->options);
        unset($this->options); // ne pas faire apparaître les options dans les resultats
        if ($validate instanceof errorObject) $this->status = $validate;
        else {
            if ($data instanceof errorObject) $this->setError($data);
            else {
                $this->response['data'] = $data['filteredData'];
                $this->response['total'] = (int)$data['globalcount'];
                $this->status = new errorObject(errorObject::SUCCESS_REQUEST_COMPLETED);
            }
        }
    }

    /**
     * @param $error
     */
    public function setError($error)
    {
        unset($this->options); // ne pas faire apparaître les options dans les resultats
        $this->status = $error;
    }

    /**
     * Gère les paramètres passés en POST
     * @static
     * @return $optionsArray array d'options
     */
    protected function _getOptionArray()
    {
        $optionsArray = array("page" => $this->_getOptionPageFilterStatus(), "itemsPage" => $this->_getOptionNbPerPageFilterStatus(), "total" => 0,
            // // will be updated afterward
            "access_token" => $this->_getOptionToken(), "author" => 'Jack Lestrohan', "owner" => 'boutique.aero', "copyright" => '2014 - boutique.aero', "api_version" => self::API_VERSION, "section" => "not defined");
        return $optionsArray;
    }

    /**
     * définit le num de page par défaut lorsque celui-ci n'est pas parametré
     * @static
     * @return Booléan
     */
    protected function _getOptionPageFilterStatus()
    {
        $getpage = 0;
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                $getpage = (int)filter_input(INPUT_POST, 'page', FILTER_SANITIZE_NUMBER_INT);
                break;
            case 'GET':
                $getpage = (int)filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
                break;
            default:
                $getpage = (int)filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
                break;
        }

       return ($getpage == 0) ? 1 : $getpage;
    }

    /**
     * Définit si itemsPage sera utilisé à sa valeur défaut ou non
     * @static
     * @return Booléan
     */
    protected function _getOptionNbPerPageFilterStatus()
    {
        $nbPerpage = 0;
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                $nbPerpage = (int)filter_input(INPUT_POST, 'itemsPage', FILTER_SANITIZE_NUMBER_INT);
                break;
            case 'GET':
                $nbPerpage = (int)filter_input(INPUT_GET, 'itemsPage', FILTER_SANITIZE_NUMBER_INT);
                break;
            default:
                $nbPerpage = (int)filter_input(INPUT_GET, 'itemsPage', FILTER_SANITIZE_NUMBER_INT);
                break;
        }

        return ($nbPerpage == 0) ? self::DEFAULT_ITEMS_PER_PAGE : $nbPerpage;
    }

    /**
     * Définit le token d'access (client-token)
     * @static
     * @return Booléan true si parametre token a été inclus dans la requête
     */
    protected function _getOptionToken()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') return (isset($_POST['token']));
        return (isset($_GET['token']));
    }

    /**
     * Vérifie la validité du token envoyé en paramètre
     * @static
     * @return Booléan true si token valide
     */
    protected function _verifyToken($token)// int
    {
        return ($token == self::CLIENT_TOKEN_VALUE);
    }

    /**
     * Méthode de validation des options
     * @static
     * @return Booléan true si token valide
     */
    protected function _validate_options($options)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') $tokenData = isset($_POST['token']) ? $_POST['token'] :  null;
        else $tokenData = isset($_GET['token']) ? $_GET['token'] : null;

        // token ?
        if (!self::_getOptionToken())
            return new errorObject(errorObject::ERROR_MISSING_ACCESS_TOKEN);
        else if (!self::_verifyToken((int)$tokenData))
            return new errorObject(errorObject::ERROR_INVALID_ACCESS_TOKEN);

        return true;
    }
}
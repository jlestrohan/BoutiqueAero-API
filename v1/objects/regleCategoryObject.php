<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 31/07/2014
 * Time: 17:00
 */

class regleCategoryObject
{

    /**
     * @var
     */
    public $categoryid;

    /**
     * @var
     */
    public $categorylibelle;


    /**
     * @param $record
     */
    public function __construct($record)
    {
        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            $this->categoryid = (int)$record->categoryid;

            $this->categorylibelle =  preg_replace('/<[^>]*>/', ' ', utf8_encode($record->categorylibelle));
        }
    }
}

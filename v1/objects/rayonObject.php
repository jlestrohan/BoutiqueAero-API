<?php
/**
/*
 *
 * Rayon object
 *
 * @package    rayonObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */


class rayonObject
{
    /**
     * Id de rayon
     * @var int $rayonID id du rayon
     */
    public $rayonID;

    /**
     * Libellé du rayon
     * @var string $rayonLibelle libellé du rayon
     */
    public $rayonLibelle;

    /**
     * Descriptif du rayon
     * @var string $rayonDescriptif descriptif du rayon
     */
    public $rayonDescriptif;


    /**
     * @param $record
     */
    public function __construct($record)
    {
        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            $this -> rayonID = (int)$record->rayonid;
            $this -> rayonDescriptif = utf8_encode($record->rayondescriptif);
            $this -> rayonLibelle = utf8_encode($record->rayonlibelle);
        }
    }
}
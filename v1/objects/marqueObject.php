<?php
/**
/*
 *
 * Marque object
 *
 * @package    marqueObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */


class marqueObject
{
    /**
     * Constante donnant le chemin de base des images marque
     * @static const IMAGE_BASE_URL
     */
    const IMAGE_BASE_URL = "http://images.boutique.aero/logos";

    /**
     * Id de marque
     * @var int $idMarque id de la marque
     */
    public $marqueid;

    /**
     * Libellé de la marque
     * @var string $marqueLibelle libellé de la marque
     */
    public $marquelibelle;

    /**
     * logoUrl
     * @var string url du logo de la marque sur le site
     */
    public $logourl;


    /**
     *
     * @param Marques $record
     */
    public function __construct($record)
    {
        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            $this -> marqueid = (int)$record->marqueid;
            $this -> marquelibelle = utf8_encode($record->marquelibelle);
            $this -> logourl = self::IMAGE_BASE_URL . '/m-'.$record->marqueid.'.png';
        }
    }
}


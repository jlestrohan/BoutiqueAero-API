<?php
/**
/*
 *
 * Gamme object
 *
 * @package    gammeObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */


class gammeObject
{
    /**
     * Id de gamme
     * @var int $idGamme id de la gamme
     */
    public $gammeID;

    /**
     * Libellé de la gamme
     * @var string $gammeLibelle libellé de la gamme
     */
    public $gammeLibelle;

    /**
     * Descriptif de la gamme
     * @var string $gammeDescriptif descriptif de la gamme
     */
    public $gammeDescriptif;

    /**
     * Id du rayon parent
     * @var int $id du rayon parent
     */
    public $rayonID;

    /**
     * @param $record
     */
    public function __construct($record)
    {
;        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            $this -> gammeID = (int)$record->gammeid;
            $this -> rayonID = (int)$record->rayonid;
            $this -> gammeLibelle = utf8_encode($record->gammelibelle);
            $this -> gammeDescriptif = utf8_encode($record->gammedescriptif);
        }
    }
}


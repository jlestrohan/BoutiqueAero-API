<?php
	/**
	 /*
	 * Produit object
	 *
	 * @package    produitObject.php
	 * @author     Jack Lestrohan
	 * @copyright  2014 boutique.aero
	 * @license    All rights protected
	 * @version    boutique.aero API v1
	 * @link       http://www.boutique.aero/api/v1/
	 *
	 */

    require_once('models/Tva.php');

	class produitObject
	{
		/**
		 * Constante donnant le chemin de base des images produits
		 * @static const IMAGE_BASE_URL
		 */
		const IMAGE_BASE_URL = "http://images.boutique.aero";
		
		/**
		 * Constantes décrivant les différentes possibilités de Disponibilités produits
		 * @static const PRODUCT_AVAILABILITY_
		 */
		const PRODUCT_AVAILABILITY_DISPONIBLE = 0;
		const PRODUCT_AVAILABILITY_SUR_COMMANDE = 1;
		const PRODUCT_AVAILABILITY_UNIQUEMENT_EN_MAGASIN = 2;
		const PRODUCT_AVAILABILITY_INDISPONIBLE_TEMPORAIREMENT = 3;
		const PRODUCT_AVAILABILITY_COMMERCIALISATION_ARRETEE = 4;
		const PRODUCT_AVAILABILITY_COMMERCIALISATION_SUSPENDUE = 5;
		const PRODUCT_AVAILABILITY_PRESTATION = 6;
		const PRODUCT_AVAILABILITY_PRODUIT_COMPOSANT_UNIQUEMENT = 7;
		const PRODUCT_AVAILABILITY_EN_COURS_DE_REFERENCEMENT = 8;
		const PRODUCT_AVAILABILITY_INDISPONIBLE_DEFINITIVEMENT = 9;
		
		/**
		 * Numéro de commande
		 * @var int $idCommande id de la commande
		 */
		public $produitID;
		
		/**
		 * Libellé du produit
		 * @var string $produitLibelle libellé du produit
		 */
		public $produitLibelle;
		
		/**
		 * designation
		 * @var string $produitDesignation désignation du produit
		 */
		public $produitDesignation;
		
		/**
		 * $produitShortDescription
		 * @var string $produitShortDescription courte description du produit
		 */
		public $produitShortDescription;
		
		/**
		 * $produitLongDescription
		 * @vvar string $produitLongDescription longue description du produit
		 */
		public $produitLongDescription;
		
		/**
		 * id de la gamme
		 * @var int $gammeID id de la gamme
		 */
		public $gammeID;
		
		/**
		 * produitPrixSiteTTC
		 * @var float produitPrixSiteTTC prix TTC affiché
         * Prix site du produit
		 */
		public $produitPrixSiteTTC;

		/**
		 * codebar
		 * @var string $codebar codebarre du produit
		 */
		public $codebar;
		
		/**
		 * tva
		 * @var float $tva tva du produit
		 */
		public $taux;
		
		/**
		 * disponibilité
		 * @string disponibilité du produit
		 */
		public $produitDisponibilite;

        /**
         * isNew
         * @bool isNew flag true si produit tout nouveau tout beau
         */
        public $isNew;

        /**
         * id de la marque
         * @int disponibilité du produit
         */
        public $marqueID;

		/**
		 * Liens vers les images
		 * @var array $imagesUrls array d'urls des images du produit
		 */
		 public $imagesUrls;

        /**
         * @int produitStockActuel nombre en stock
         */
        public $produitStockActuel;

        /**
         * @float produitPoids poids actuel du produit
         */
        public $produitPoids;


        /**
         * @param $record
         */
        public function __construct($record)
		{
			if (empty($record))
				throw new Exception('Construction de l\'objet impossible');
			else {
				$this -> produitID = (int)$record->produitid;
                $this -> produitLibelle = utf8_encode($record->produitlibelle);
                $this -> isNew = (bool)$record->isnew;
                $this -> gammeID = (int)$record->gammeid;
                $this -> produitDesignation = utf8_encode($record->produitdesignation);
                $this -> produitShortDescription = utf8_encode($record->produitshortdescription);
                $this -> produitLongDescription = utf8_encode($record->produitlongdescription);
                $this -> produitPrixSiteTTC = (float)$record->produitprixsitettc;
                $this -> marqueID = (int)$record->marqueid;
                $this -> taux =  (float)$record->taux; //$this->_getProductTaux((int)$record->produitid);
                $this -> produitDisponibilite = $this->_getProductDisponibilityString($record->produitdisponibilite);
                $this -> produitStockActuel = (int)$record->produitstockactuel;
                $this -> imagesUrls = $this->getAllImagesForProductId($record->produitid);
                $this -> produitPoids = (float)$record->produitpoids;

			}
		}

		/**
		 * Numéro de commande
		 * @static getAllImagesForProductId($id)
		 * @param int $id id du produit
		 * @return array $result array contenant les URL des imagesdu produit associé
		 */
		public function getAllImagesForProductId($id)
		{
            // cette fonction itere le nombre de fichiers présent dans chaque répertoire d'image ($id) et compte
            // le nombre de fichiers de chaque type pour en déduire toutes les url des images

            // http://www.cowburn.info/2010/04/30/glob-patterns/
            //var_dump(fnmatch(self::IMAGE_BASE_URL . '/?*/p-?*.jpg', self::IMAGE_BASE_URL . '/' . $id . '/p-' . $id . '.jpg'));

            // compte le nombre de fichier répondant au pattern grand-p-110-1.jpg
            if (file_exists('/var/www/boutique/illustrations/' . $id .'/')) { // file exists ?
                $dir = new DirectoryIterator('/var/www/boutique/illustrations/' . $id .'/');

                $patternArray = array(   "p-?*-?*.jpg",
                                "petit-p-?*-?*.jpg",
                                "moyen-p-?*-?*.jpg",
                                "grand-p-?*-?*.jpg",
                                "maxi-p-?*-?*.jpg"); // array de patterns pour iteration (size ASC)

                // for ($i=0; $i < count($patternArray); $i++) { // iteration sur le nombre de patterns possible
                //TODO: iterer sur tous les fichiers, pour le moment on en vérifie un seul. Si par exemple le premier
                // pattern renvoie 5 iterations, on considère qu'il y aura 5 images de chaque type dans le répertoire
                    $filecount = 0;
                    foreach ($dir as $fileinfo) {
                        if (!$fileinfo->isDot()) {
                            if (fnmatch($patternArray[1], $fileinfo->getFilename())) {
                                $filecount++;
                            }
                        }
                    }
                    //var_dump('for match ' .$patternArray[$i]. ' we got ' . $filecount . 'files');
                $result = array();

                for ($i=0; $i<$filecount; $i++) {
                    $j=$i+1;
                    $result[] = array(  "setNumber" => $j,
                                    "image_grand_url" => self::IMAGE_BASE_URL . '/' . $id . '/grand-p-' . $id . '-'.$j.'.jpg',
                                    "image_maxi_url" => self::IMAGE_BASE_URL . '/' . $id . '/maxi-p-' . $id . '-'.$j.'.jpg',
                                    "image_moyen_url" => self::IMAGE_BASE_URL . '/' . $id . '/moyen-p-' . $id . '-'.$j.'.jpg',
                                    "image_origin_url" => self::IMAGE_BASE_URL . '/' . $id . '/p-' . $id . '-'.$j.'.jpg');
                }

			    return $result;
            } else return null;
		}

		/**
		 * _getProductDisponibilityString
		 * @param int $dispo dispo du produit
		 * @return string $result string contenant la mention de disponibilité
		 */
		private function _getProductDisponibilityString($dispo)
		{
			switch ($dispo) {
				case self::PRODUCT_AVAILABILITY_DISPONIBLE:	return "disponible"; break;
				case self::PRODUCT_AVAILABILITY_SUR_COMMANDE: return "sur commande"; break;
                case self::PRODUCT_AVAILABILITY_UNIQUEMENT_EN_MAGASIN: return "disponible uniquement en magasin"; break;
				case self::PRODUCT_AVAILABILITY_INDISPONIBLE_TEMPORAIREMENT: return "temporairement indisponible"; break;
                case self::PRODUCT_AVAILABILITY_COMMERCIALISATION_ARRETEE: return "commercialisation arrêtée"; break;
                case self::PRODUCT_AVAILABILITY_COMMERCIALISATION_SUSPENDUE: return "commercialisation suspendue"; break;
                case self::PRODUCT_AVAILABILITY_PRESTATION: return "prestation"; break;
                case self::PRODUCT_AVAILABILITY_PRODUIT_COMPOSANT_UNIQUEMENT: return "composant"; break;
                case self::PRODUCT_AVAILABILITY_EN_COURS_DE_REFERENCEMENT: return "en cours de référencement"; break;
                case self::PRODUCT_AVAILABILITY_INDISPONIBLE_DEFINITIVEMENT: return "indisponible définitivement"; break;
			}
		
		}

        private function _getProductTaux($idProduct)
        {
            try {
                $tvaModelObject = Tva::find((int)$idProduct);
            } catch (Exception $e) {
                return null;
            }

            return $tvaModelObject;
        }
	}


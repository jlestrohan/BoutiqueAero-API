<?php
/**
/*
 * Produit object
 *
 * @package    commandeElementsObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */


class commandeElementsObject
{
    /**
     * ID du produit concerné
     * @var
     */
    public $produitID;

    /**
     * Prix unitaire de l'élément de commande concerné
     * @var float $articlePrixUnitaire
     */
    public $articlePrixUnitaire;

    /**
     * Quantité de ce même element inclus dans la commande
     * @var
     */
    public $quantite;

    /**
     * @var
     */
    //public $tauxTva;

    /**
     * @var
     */
    //public $idTVA;

    /**
     * @param $record
     */
    public function __construct($record)
    {
        if ($record instanceof CommandesElements) {
            $this -> produitID = (int)$record->produitid;
            $this -> articlePrixUnitaire = (float)$record->commandeprixunitaire;
            $this -> quantite = (int)$record->quantite;
            $this -> idTVA = 2;
            //$this -> tauxTva = 19.6;
        } elseif (is_array($record)) {
            $this -> produitID = (int)$record['productId'];
            $this -> articlePrixUnitaire = (float)$record['prixUnitaire'];
            $this -> quantite = (int)$record['quantite'];
            //$this -> tauxTva = (float)$record['tauxTva'];
        }
    }

    /**
     * Check l'objet courant et retourne true s'il est conforme
     * @return bool|errorObject
     */
    public function validateObject()
    {
        if (!isset($this->produitID)) return new errorObject(errorObject::ERROR_COMMANDE_ELEMENT_ID_NOT_SET);
        if ($this->produitID == 0) return new errorObject(errorObject::ERROR_COMMANDE_ELEMENTS_ZERO_ID);
        if ($this->quantite == 0) return new errorObject(errorObject::ERROR_COMMANDE_ELEMENT_ZERO_QUANT);

        return true;
    }

    public function getPrixTTC()
    {
        return $this->articlePrixUnitaire * $this->quantite + ($this->articlePrixUnitaire * $this->quantite * $this->tauxTva/100);
    }

    public function getPrixHT()
    {
        return $this->articlePrixUnitaire * $this->quantite;
    }

    public function getTvaAmount()
    {
        return $this->articlePrixUnitaire * $this->quantite * $this->tauxTva/100;
    }
}
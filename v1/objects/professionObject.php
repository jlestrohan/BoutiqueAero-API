<?php
/**
 * Created by PhpStorm.
 * User: jack
 * Date: 22/08/2014
 * Time: 17:29
 */

class professionObject
{
    /**
     * Id de profession
     * @var int $professionID id de la profession
     */
    public $professionID;

    /**
     * Libellé de la profession
     * @var string $professionLibelle libellé de la profession
     */
    public $professionLibelle;


    /**
     * @param $record
     */
    public function __construct($record)
    {
        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            $this -> professionID = (int)$record->professionid;
            $this -> professionLibelle = utf8_encode($record->profession);
        }
    }
}
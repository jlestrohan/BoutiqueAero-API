<?php
/**
/*
 *
 * User object
 *
 * @package    userObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

require_once (dirname(__FILE__) . '/../models/Professions.php');

class userObject
{

    /**
     * Id de client
     * @var int clientID id client
     */
    public $clientID;

    /**
     * @var string nom client
     */
    public $clientNom;

    /**
     * @var string prénom client
     */
    public $clientPrenom;

    /**
     * @var string adresse1 client
     */
    public $clientAdresse1;

    /**
     * @var string adresse2 client
     */
    public $clientAdresse2;

    /**
     * @var string adresse3 client
     */
    public $clientAdresse3;

    /**
     * @var string CP client
     */
    public $clientCodePostal;

    /**
     * @var string ville client
     */
    public $clientVille;

    /**
     * @var string pays client
     */
    public $clientPays;

    /**
     * @var string telephone client
     */
    public $clientTelephone;

    /**
     * @var string fax client
     */
    public $clientFax;

    /**
     * @var string portable client
     */
    public $clientPortable;

    /**
     * @var string email client
     */
    public $clientEmail;

    /**
     * @var string dateCreation client
     */
    public $dateCreation;

    /**
     * @var bool liste cadeaux active
     */
    public $clientListeCadeauxStatut;

    /**
     * @var array profession client
     */
    public $professions;


    /**
     * @param $record
     */
    public function __construct($record)
    {
        if (empty($record))
            throw new Exception('Construction de l\'objet impossible');
        else {
            $this -> clientID = (int)$record->clientid;
            $this -> clientListeCadeauxStatut = (bool)$record->clientlistecadeauxstatut;
            $this -> clientNom = utf8_encode($record->clientnom);
            $this -> clientPrenom = utf8_encode($record->clientprenom);
            $this -> clientAdresse1 = utf8_encode($record->clientadresse1);
            $this -> clientAdresse2 = utf8_encode($record->clientadresse2);
            $this -> clientAdresse3 = utf8_encode($record->clientadresse3);
            $this -> clientEmail = utf8_encode($record->clientemail);
            $this -> clientVille = utf8_encode($record->clientville);
            $this -> clientPays = utf8_encode($record->clientpays);
            $this -> clientTelephone = utf8_encode($record->clienttelephone);
            $this -> clientFax = utf8_encode($record->clientfax);
            $this -> clientPortable = utf8_encode($record->clientportable);
            $this -> dateCreation = $record->datecreation;
            $this -> clientListeCadeauxStatut = (bool)$record->clientlistecadeauxstatut;
            $this -> professions = $this->_getProfessions($record);
        }
    }

    // récupère une array de professions concernant l'user courant
    private function _getProfessions($record)
    {
        // eh ouais, les ID sont stockés dans la base sous la forme (accroche-toi johnny... de champ TEXT d'ids séparés par des ':'..
        // Miraculeux non ? le summum de l'informatique, la panacée de l'incroyable histoire du code qu'il est supaÿr...
        // donc ben on est obligé de faire avec cette merveille de technologie, ensuite on utilisera du bitmask..

        // extraction du champ 'TEXT'
        $textProfessions = $record->profession;
        // et on explode cette variable pour obtenir une array d'ids... whoaou genial kesk'on s'éclate!
        $explodedShit = explode(':', $textProfessions);

        // ah on revient enfin aux fondamentaux: la programmation!
        $arrayProfs = array();
        foreach($explodedShit as $shit) {
            $tmpVar = Professions::find($shit);
            $arrayProfs[] = utf8_encode($tmpVar->profession);
        }

        return $arrayProfs;
    }

}
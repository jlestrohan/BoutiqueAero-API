<?php
/**
/*
 *
 * User object
 *
 * @package    livraisonAdressObject.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    boutique.aero API v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

class livraisonAdressObject
{
    /**
     * @var int id du client
     */
    public $clientID;

    /**
     * @var int id d'adresse
     */
    public $livraisonID;

    /**
     * @var
     */
    public $livraisonCivilite;

    /**
     * @var string nom user de l'adresse livraison
     */
    public $livraisonNom;

    /**
     * @var string prenom user de l'adresse de livraison
     */
    public $livraisonPrenom;

    /**
     * @var string adresse 1
     */
    public $livraisonAdresse1;

    /**
     * @var string adresse 2
     */
    public $livraisonAdresse2;

    /**
     * @var string adresse 3
     */
    public $livraisonAdresse3;

    /**
     * @var string CP
     */
    public $livraisonCodePostal;

    /**
     * @var string ville
     */
    public $livraisonVille;

    /**
     * @var string pays
     */
    public $livraisonPays;

    /**
     * @var string libelle de l'adresse
     */
    public $livraisonTitre;

    /**
     * @param $record
     */
    public function __construct($record = null)
    {
        if (!empty($record)) {
            // selon le type d'objet en entrée, on populate différement
            if ($record instanceof Livraison) {
                $this -> clientID = (int)$record->clientid;
                $this -> livraisonID = (int) $record->livraisonid;
                $this -> livraisonNom = utf8_encode($record->livraisonnom);
                $this -> livraisonPrenom = utf8_encode($record->livraisonprenom);
                $this -> livraisonAdresse1 = utf8_encode($record->livraisonadresse1);
                $this -> livraisonAdresse2 = utf8_encode($record->livraisonadresse2);
                $this -> livraisonAdresse3 = utf8_encode($record->livraisonadresse3);
                $this -> livraisonCodePostal = $record->livraisoncodepostal;
                $this -> livraisonVille = utf8_encode($record->livraisonville);
                $this -> livraisonPays = utf8_encode($record->livraisonpays);
                $this -> livraisonTitre = utf8_encode($record->livraisontitre);
            }
            else if (is_array($record)) {
                $this->livraisonNom = $record['nom'];
                $this->livraisonPrenom = $record['prenom'];
                $this->livraisonAdresse1 = $record['adress1'];
                $this->livraisonAdresse2 = $record['adress2'];
                $this->livraisonAdresse3 = $record['adress3'];
                $this->livraisonCodePostal = $record['cp'];
                $this->livraisonVille = $record['ville'];
                $this->livraisonPays = $record['pays'];
                $this->livraisonTitre = $record['livraison_titre'];
            }

        } else return new errorObject(errorObject::ERROR_INVALID_PARAMETER_VALUE);
    }

}
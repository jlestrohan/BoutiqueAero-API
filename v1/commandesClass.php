<?php
/**
 * Objet de réponse routing API pour les commandes
 *
 * @package    commandesClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 *
 * Gère les commandes user
 *
 */

require_once(__DIR__ . '/produitsClass.php');
require_once(__DIR__ . '/objects/commandeObject.php');
require_once(__DIR__ . '/objects/commandeElementsObject.php');
require_once(__DIR__ . '/models/Commandes.php');

// under PHP 5.4 this function must be manually implemented
if (!function_exists('array_column')) {
    include_once(__DIR__ . '/php54/array_column.php');
}


class commandesClass
{
    /**
     * section API
     */
    const SECTION = "commandes";

    /**
     * Point d'entr�e de la section Commandes
     * @return array $returnable contenant la reponse JSON
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // POST NEW COMMANDE
        if (isset($_POST['new_order'])) {
            // nouvelle commande, vérifions la validite des parametres
            if (!isset($_POST['order_comment']) or
                !isset($_POST['elements']) or
                !isset($_POST['frais_port']) or
                !isset($_POST['parcelAddress'])
            ) {
                $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
                return $returnable;
            }
            if (!is_array($_POST['elements'])) {
                $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));
                return $returnable;
            }

            $returnable->setData(self::setCommande($_POST));

        } elseif (isset($_POST['delete_order'])) {
            if (!isset($_POST['order_id'])) {
                $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
                return $returnable;
            } elseif ($_POST['order_id'] == 0) {
                $returnable->setError(new errorObject(errorObject::ERROR_INVALID_PARAMETER_VALUE));
            } else {
                $returnable->setData(self::deleteOrder($_POST['order_id']));
            }
        } else {
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }

        return $returnable;
    }

    /**
     * @return responseObject
     */
    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // COMMANDE PAR ID
        if ($retGetId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
            $returnable->setData(self::getCommandeById($retGetId));
        } // COMMANDES PAR ID CLIENT
        else if ($retGetId = filter_input(INPUT_GET, 'idClient', FILTER_SANITIZE_NUMBER_INT)) {
            $returnable->setData(self::getCommandeByIdClient($retGetId, $returnable->options));
        } // COMMANDE PAR ID
        else if ($retGetId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
             $returnable->setData(self::getCommandeById($retGetId));
        } else {
            // par défaut on demande un parametre sinon on colle une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }


        return $returnable;
    }

    /**
     * Recherche une commande a partir de son id
     *
     * @param string $id id de la commande a chercher
     *
     * @return array contenant les infos de la commande d�sign�e
     */
    private static function getCommandeById($id)
    {
        try {
            $row = Commandes::find($id);
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if ($row instanceof Commandes) {
            $object = new commandeObject($row);
            return array("globalcount" => 1, "filteredData" => $object);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * Retourne une array contenant les differentes commandes associées
     * à un id Client
     *
     * @param int $clientId  id client
     * @param array $options array d'options
     *
     * @return array $data contenant la reponse JSON
     */
    private static function getCommandeByIdClient($clientId, $options)
    {
        $conditions = array('clientID = ' . $clientId);

        try {
            $rows = Commandes::all(array(
                'conditions' => $conditions,
                'order'      => 'commandeDate desc',
                'limit'      => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
            ));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $count = Commandes::count(array('conditions' => $conditions));
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        if (!empty($rows)) {
            $commandes_array = array();
            foreach ($rows as $commande) {
                $commandes_array[] = new commandeObject($commande);
            }
            return array("globalcount" => $count, "filteredData" => $commandes_array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * Vérifie la commande, les articles, et retourne le cas échéant une erreur si probleme
     *
     * @param array $elements
     * @param $fraisPort
     * @param $montantTTC
     * @param $livraisonAdress
     * @param $flagColis
     * @param $commentairesCommandes
     *
     * @return array|bool|errorObject
     */
    private static function setCommande($post)
    {
        // site interroger mac adress: http://www.coffer.com/mac_find/

        if (empty($post['elements'])) return new errorObject(errorObject::ERROR_ORDER_NO_ARTICLE);

        // CREE LA NOUVELLE COMMANDE (pour récup l'ID)
        // Démarrage des transactions, commits ou fallback/rollback
       // $c1 = Commandes::connection();
       // $c2 = CommandesElements::connection();
       // $c3 = CommandesLivraison::connection();

        try {
            //$c1->transaction();
            //$c2->transaction();
           //$c3->transaction();
            $new_commande = Commandes::create(array(
                'clientid'     => $post['client_id'],
                'clientip'     => $post['mac_adress'],
                'paiementmode' => 0, //@TODO: bouchon
                'port'         => $post['frais_port'],
                'apayer'       => array_sum(array_column($post['elements'], 'commandePrixUnitaire')) * array_sum(array_column($post['elements'], 'quantite'))
            ));

           foreach ($post['elements'] as $element) {
                CommandesElements::create(array(
                    'produitid'           => $element['produitId'],
                    'commandeid'           => $new_commande->commandeid,
                    'quantite'             => $element['quantite'],
                    'commandeprixunitaire' => $element['commandePrixUnitaire']
                ));
            }

            CommandesLivraison::create(array(
                'commandeid'          => $new_commande->commandeid,
                'livraisoncivilite'   => 0, //@TODO: bouchon
                'livraisonnom'        => $post['parcelAddress']['nom'],
                'livraisonprenom'     => $post['parcelAddress']['prenom'],
                'livraisonadresse1'   => $post['parcelAddress']['adress1'],
                'livraisonadresse2'   => $post['parcelAddress']['adress2'],
                'livraisonadresse3'   => $post['parcelAddress']['adress3'],
                'livraisoncodepostal' => $post['parcelAddress']['cp'],
                'livraisonville'      => $post['parcelAddress']['ville'],
                'livraisonpays'       => $post['parcelAddress']['pays'],
            ));

           // $c1->commit();
           /// $c2->commit();
           // $c3->commit();
        } catch (\Exception $e) {
           // $c1->rollback();
            //$c2->rollback();
           // $c3->rollback();
            throw $e;
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }


        $qté__total_articles = array_sum(array_column($post['elements'], 'quantite'));

        // termine d'update la commande


        //$flagColis = 1;

        //$commentaire = 'this is a test comment on the order';

        // ENREGISTRE LA COMMANDE DANS LA BASE

        $result = array(
            'order_id'          => (int)$new_commande->commandeid,
            'order_date'        => date('Y-m-d H:i:s'),
            'order_nb_articles' => $qté__total_articles,
            //'order_tva_amount' => $total_amount_tva,
            //'order_amount_ttc' => (float)number_format($prix_total_ttc,2),
            //'order_amount_ht' => (float)number_format($prix_total_ht,2)
        );

        return array("globalcount" => count($post['elements']), "filteredData" => $result);
    }


    protected static function deleteOrder($orderNumber)
    {
        // efface la commande indiquée ainsi que les éléments
        try {
            $commande = Commandes::find($_POST['order_id']);
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        try {
            $commande->delete();
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }


        $elements = CommandesElements::find_all_by_commandeid($_POST['order_id']);
        foreach ($elements as $element) {
            $element->delete();
            return new errorObject(errorObject::SUCCESS_REQUEST_COMPLETED);
        }

    }
}

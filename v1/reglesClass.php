<?php
/**
 * Objet de r�ponse routing API pour les produits
 *
 * @package   gammesClass.php
 * @author     Jack Lestrohan
 * @copyright  2014 boutique.aero
 * @license    All rights protected
 * @version    v1
 * @link       http://www.boutique.aero/api/v1/
 *
 */

require_once (__DIR__ . '/objects/regleObject.php');
require_once (__DIR__ . '/objects/regleCategoryObject.php');
require_once (__DIR__ . '/objects/responseObject.php');
require_once (__DIR__ . '/models/Regles.php');
require_once (__DIR__ . '/models/ReglesCategories.php');

class reglesClass
{
    /**
     * section API
     */
    const SECTION = "regles";


    /**
     * Point d'entr�e de la section Regles.php
     * @return array $returnable contenant la reponse JSON
     */
    public static function apiPost()
    {
        $returnable = new responseObject(self::SECTION);

        // room for more POST requests if any
        // par d�faut on demande un parametre sinon on colle une erreur
        $returnable->setError(new errorObject(errorObject::ERROR_ACTION_NOT_AUTHORIZED));

        return $returnable;
    }

    public static function apiGet()
    {
        $returnable = new responseObject(self::SECTION);

        // LIST ALL CATEGORIES
        if (isset($_GET['listReglesCategories'])) {
            $returnable->setData(self::getReglesCategoriesList($returnable->options));
        }

        // RECHERCHE PAR ID
        elseif ($retGetId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT)) {
            $returnable->setData(self::getRegleById($retGetId));
        }

        // LISTE PAR CATEGORIE
        elseif ($retGetCategoryID = filter_input(INPUT_GET, 'categoryId', FILTER_SANITIZE_NUMBER_INT)) {
            $returnable->setData(self::getReglesListByCategoryID($retGetCategoryID, $returnable->options));
        } else {
            // aucun parametre 'command' on renvoie donc une erreur
            $returnable->setError(new errorObject(errorObject::ERROR_REQUIRED_PARAMETER_MISSING));
        }

        return $returnable;
    }

    /**
     * @param $id
     * @return array|errorObject
     */
    private static function getRegleById($id)
    {
        try {
            $data = Regles::find((int)$id);
        } catch (\ActiveRecord\RecordNotFound $e) {
            return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
        } catch (Exception $e) {
            return new errorObject(errorObject::ERROR_UNKNOWN_ERROR);
        }

        $regle_object = new regleObject($data);
        $count = isset($regle_object) ? 1 : 0;
        return array("globalcount" => $count, "filteredData" => $regle_object);
    }

    /**
     * @param $categoryID
     * @param $options
     * @return array|errorObject
     */
    private static function getReglesListByCategoryID($categoryID, $options)
    {
        $rows = Regles::all(array(
                'conditions' => array('category_id = ?', (int)$categoryID),
                'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
            )
        );

        $count = Regles::count(array('conditions' => array('category_id = ?', (int)$categoryID)));

        if (!empty($rows)) {
            $array = array();
            foreach ($rows as $row) {
                $array[] = new regleObject($row);
            }
            return array("globalcount" => $count, "filteredData" => $array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }

    /**
     * @param $options
     * @return array|errorObject
     */
    private static function getReglesCategoriesList($options)
    {
        $rows = ReglesCategories::all(array(
            'limit' => (int)$options['itemsPage'], 'offset' => ($options['page'] - 1) * (int)$options['itemsPage']
        ));

        $count = ReglesCategories::count();

        if (!empty($rows)) {
            $array = array();
            foreach ($rows as $row) {
                $array[] =  new RegleCategoryObject($row);
            }
            return array("globalcount" => $count, "filteredData" => $array);
        } else return new errorObject(errorObject::ERROR_RECORD_NOT_FOUND);
    }
}